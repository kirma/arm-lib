import matplotlib.pyplot as plt
import numpy as np
import matplotlib.pyplot as plt
import time

# we have to have non-zero starting velocity. Otherwise it got stuck in the startting position of trajectory forever
from numpy import sign

min_start_velocity = 0.03

#https://github.com/AtsushiSakai/PythonRobotics
class QuinticPolynomial:

    def __init__(self, xs, vxs, axs, xe, vxe, axe, time):
        # calc coefficient of quintic polynomial
        # See jupyter notebook document for derivation of this equation.
        self.a0 = xs
        self.a1 = vxs
        self.a2 = axs / 2.0

        A = np.array([[time ** 3, time ** 4, time ** 5],
                      [3 * time ** 2, 4 * time ** 3, 5 * time ** 4],
                      [6 * time, 12 * time ** 2, 20 * time ** 3]])
        b = np.array([xe - self.a0 - self.a1 * time - self.a2 * time ** 2,
                      vxe - self.a1 - 2 * self.a2 * time,
                      axe - 2 * self.a2])
        x = np.linalg.solve(A, b)

        self.a3 = x[0]
        self.a4 = x[1]
        self.a5 = x[2]

    def calc_point(self, t):
        xt = self.a0 + self.a1 * t + self.a2 * t ** 2 + \
             self.a3 * t ** 3 + self.a4 * t ** 4 + self.a5 * t ** 5

        return xt

    def calc_first_derivative(self, t):
        xt = self.a1 + 2 * self.a2 * t + \
             3 * self.a3 * t ** 2 + 4 * self.a4 * t ** 3 + 5 * self.a5 * t ** 4

        return xt

    def calc_second_derivative(self, t):
        xt = 2 * self.a2 + 6 * self.a3 * t + 12 * self.a4 * t ** 2 + 20 * self.a5 * t ** 3

        return xt

    def calc_third_derivative(self, t):
        xt = 6 * self.a3 + 24 * self.a4 * t + 60 * self.a5 * t ** 2

        return xt


def servo_quintic_planner(xs, vxs, axs, xe, vxe, axe, max_accel, max_jerk, tm_min, tm_max, dt):

    # try 16 solutions for different time length until constrains are broken. If no solution, return with longest time
    for tm in np.arange(tm_min, tm_max, (tm_max-tm_min)/8):
        # print(f"trajectory in {tm:3F} s")
        xqp = QuinticPolynomial(xs, vxs, axs, xe, vxe, axe, tm)
        tim, rx, rv, ra, rj = [], [], [], [], []
        for t in np.arange(0.0, tm, dt):
            tim.append(t)
            x = xqp.calc_point(t)
            rx.append(x)
            vx = xqp.calc_first_derivative(t)
            rv.append(vx)
            ax = xqp.calc_second_derivative(t)
            ra.append(ax)
            jx = xqp.calc_third_derivative(t)
            rj.append(jx)

        if max([abs(i) for i in ra]) <= max_accel and max([abs(i) for i in rj]) <= max_jerk:
            # print(f"Found path - {tm:3F} s")
            break

    # check if we have any solution
    try:
        a = len(tim)
    except:
        print("No solution found!")
        tim, rx, rv, ra, rj = [0.], [0.], [0.], [0.], [0.]

    # correct starting velocities - must not be 0 or too small. check 1/10 of the path
    sn = sign(rv[0])
    if sn == 0.0:
        sn = sign(rv[2])
    if abs(rv[0]) < min_start_velocity:
        # we're checking the sign of the future trajectory far away in case if it changes the sign very soon, eg:
        # -0.01 0.0  0.02 0.06 ...
        sn = sign(sign(rv[2])+sign(rv[3])+sign(rv[4]))

        rv[0] = sn * min_start_velocity
    # for i in range(len(tim)//10+1):
    #     sn = sign(rv[i])
    #     if sn == 0:
    #         sn = sign(rv[i+1])
    #     if abs(rv[i]) < min_start_velocity:
    #         rv[i] = sn * min_start_velocity


    return tim, np.array(rx), np.array(rv), ra, rj


if __name__ == '__main__':
    sx = 0.90       # start elbow
    sy = 0.77       # start shoulder
    sv = 0.0        # start speed [m/s]
    sa = 0.0        # start accel [m/ss]

    gx = 0.69       # goal x position [m]
    gv = 0.0        # goal speed [m/s]
    ga = 0.1        # goal accel [m/ss]

    max_accel = 1.0 # max accel [/ss]
    max_jerk = 3.0  # max jerk [/sss]

    dt = 0.02       # time tick [s]
    tm_min = 1
    tm_max = 5.0

    st = time.time()
    ttt, x, v, a, j = servo_quintic_planner( sx, sv, sa, gx, gv, ga,  max_accel, max_jerk, tm_min, tm_max, dt)

    elp = time.time() - st
    print(f"Traj Len: {len(ttt)} Time: {elp:4f}")
    # plt.plot(v)
    # plt.show()