import math
import random
from datetime import timedelta
from time import sleep, time
from dynamixel_sdk import *

# from DynamixelSDK.python.tests.protocol2_0.bulk_read_write import groupBulkRead
from ktl import SuperTimeloop

from colorama import init, Fore
init(autoreset=True)


"""
Create a DynaBus instance. Then add servos with names. We will refer to servos by name rather than by id.
The object also creates TimeLoop for polling servo positions, currents and temperatures

Important: the FTDI driver must be tuned to have low latency (1-2ms) instead of default 16ms. 
On Mac - use adjust_latency.c
On Linux - TBD
On Windows - driver properties
"""


DEBUG_PRINT = True
SERVO_POLLING_MS = 33.


POW_2_32 = pow(2, 32)

ADDR_SHUTDOWN = {
    'X_SERIES': 63,
    'PH42_SERIES': 63,
    'PH54_SERIES': 63,
}
ADDR_ACCEL_LIMIT = {
    'X_SERIES': 108,
    'PH42_SERIES': 556,
    'PH54_SERIES': 556,
}
ADDR_PWM_LIMIT = {
    'X_SERIES': 100,
    'PH42_SERIES': 548,
    'PH54_SERIES': 548,
    }
ADDR_PRESENT_CURRENT = {
    'X_SERIES': 126,
    'PH42_SERIES': 574,
    'PH54_SERIES': 574,
}
ADDR_PRESENT_INPUT_VOLTAGE  = {
    'PH42_SERIES': 592,
    'PH54_SERIES': 592,
    'X_SERIES': 144
}
ADDR_PRESENT_TEMPERATURE = {
    'PH42_SERIES': 594,
    'PH54_SERIES': 594,
    'X_SERIES': 146
}
ADDR_VELOCITY_PROF = {
    'PH42_SERIES': 560,
    'PH54_SERIES': 560,
    'X_SERIES': 112
}
ADDR_ACCELERATION_PROF = {
    'PH42_SERIES': 556,
    'PH54_SERIES': 556,
    'X_SERIES': 108
}

ADDR_VELOCITY = {
    'PH42_SERIES': 552,
    'PH54_SERIES': 552,
    'X_SERIES': 104
}

VELOCIY_MAX = {
    'PH42_SERIES': 2920,
    'PH54_SERIES': 2920,
    'X_SERIES': 145
}

PWM_MAX = {
    'X_SERIES': 885,
    'PH42_SERIES': 2009,
    'PH54_SERIES': 2009,
}

PROF_VELOCITY_MAX = {
    'X_SERIES': 145,        # this comes from velocity limit, not max profile velocity
    'PH42_SERIES': 2920,
    'PH54_SERIES': 2920,    # 29.2 rev/min
}
PROF_ACCEL_MAX = {
    'X_SERIES': 32767,
    'PH42_SERIES': 10765,
    'PH54_SERIES': 10639,
}
VELOCITY_MAX_RADS = {
    # converted from RPMs
    'X_SERIES': 33.2 * 0.10472,
    'PH42_SERIES': 29.2 * 0.10472,
    'PH54_SERIES': 29.2 * 0.10472,
}
# ACCEL_SCALE = {
#     'X_SERIES': 6.5486,   # [rev/min2]
#     'PH42_SERIES': 1.0,
# }
ADDR_OPER_MODE = {
    'X_SERIES': 11,
    'PH42_SERIES': 11,
    'PH54_SERIES': 11,
}
ADDR_TORQUE_ENABLE = {
    'X_SERIES': 64,
    'MX_SERIES': 64,
    'PRO_SERIES': 562,
    'PH42_SERIES': 512,
    'PH54_SERIES': 512,
    'PRO_A_SERIES': 512,
    'XL320': 24
    }
ADDR_GOAL_POSITION = {
    'X_SERIES': 116,
    'MX_SERIES': 116,
    'PRO_SERIES': 596,
    'PH42_SERIES': 564,
    'PH54_SERIES': 564,
    'PRO_A_SERIES': 564,
    'XL320': 30
}
ADDR_PRESENT_POSITION = {
    'X_SERIES': 132,
    'MX_SERIES': 132,
    'PRO_SERIES': 611,
    'PH42_SERIES': 580,
    'PH54_SERIES': 580,
    'PRO_A_SERIES': 580,
    'XL320': 37
}
# DXL_MINIMUM_POSITION_VALUE = {
#     'X_SERIES': 0,
#     'MX_SERIES': 0,
#     'PRO_SERIES': -150000,
#     'PH42_SERIES': -303454,
#     'PH54_SERIES': -501433,
#     'PRO_A_SERIES': -150000,
#     'XL320': 0
# }
# DXL_MAXIMUM_POSITION_VALUE = {
#     'X_SERIES': 4095,
#     'MX_SERIES': 4095,
#     'PRO_SERIES': 150000,
#     'PH42_SERIES': 303454,
#     'PH54_SERIES': 501433,
#     'PRO_A_SERIES': 150000,
#     'XL320': 1023
# }

# open dynawisard, go to Goal Position and check +90 and -90.
DXL_N180_POSITION_VALUE = {
    'X_SERIES': 0.,  # 2048-(2048-1023)*2, #  2
    'PH42_SERIES': -151875.*2.,
    'PH54_SERIES': -250961.*2.,
}
DXL_P180_POSITION_VALUE = {
    'X_SERIES': 4095.,  # float((3073-2048)*2+2048),  WTF ???
    'PH42_SERIES': 151875.*2.,
    'PH54_SERIES': 250961.*2.,
}


class ADRPortHandler(PortHandler):
    def __init__(self, serial_port):
        PortHandler.__init__(self, serial_port)

    def setPacketTimeout(self, packet_length):
        self.packet_start_time = self.getCurrentTime()
        self.packet_timeout = (self.tx_time_per_byte * packet_length) + (16 * 2.0)

# we don't read the temperature every sync loop.
counter = 16

class DynaBus:
    def __init__(self, serial_port, baud_rate, max_temp=60.0):
        self.portHandler = ADRPortHandler(serial_port)
        self.packetHandler = PacketHandler(Protocol2PacketHandler)
        self.max_temperature = max_temp

        # Open port
        if self.portHandler.openPort() == False:
            raise Exception(f'Cannot open the Serial port {serial_port}')
        if self.portHandler.setBaudRate(baud_rate) == False:
            raise Exception('Cannot set baud rate')

        # self.bulk_read = groupBulkRead(self.portHandler, 2)

        self.servos = {}
        self.servo_types = {}
        self.servo_status = {}

        self.servo_position = {}
        # self.servo_old_position = {}
        self.servo_current = {}
        self.servo_temperature = {}
        self.servo_temp_status = {}
        self.servo_max_torque = {}

        self.desired_velocty = {}      # in servo's units

        # Timeloop object allows to periodically call a procedure with high precision and protection
        tl = SuperTimeloop()
        self.tl = tl

        @tl.job(timedelta(milliseconds=SERVO_POLLING_MS), self)
        def polling_servo_job(obj):
            global counter
            # print(f"ELAPSED: {time.time() - ttt:.3F}")
            # sometimes we want to stop the syncronous comunication or read only (e.g. to switch mode)
            if obj.do_polling == 'stop':
                return

            ttt = time.time()
            if obj.do_polling == 'all':
                # send positions to servos
                servos_to_update = obj.desired_velocty.copy()
                obj.desired_velocty={}
                for servo, dv in servos_to_update.items():
                    # print(f"{servo} -- {dv}")
                    id = obj.servos[servo]
                    tp = obj.servo_types[id]
                    dcr, dce = obj.packetHandler.write4ByteTxRx(obj.portHandler, id, ADDR_VELOCITY[tp], dv)
                    # if dce != COMM_SUCCESS:
                    #     print(f'${servo} {dce} {obj.packetHandler.getTxRxResult(dce)}')
                    #     break

            for servo in obj.servos:
                # all get/read functions save the error in status if not OK
                pos, ok = obj.get_position_from_servo(servo)
                if ok:
                    # if abs(pos - obj.servo_position[servo]) < 0.3:
                    obj.servo_position[servo] = pos
                    # if abs(obj.servo_old_position[servo] - obj.servo_position[servo]) > 0.2:
                    #     print(f"Servo Mistmatch {obj.servo_old_position[servo]:.3F} {obj.servo_position[servo]:.3F}")
                    # obj.servo_old_position[servo] = obj.servo_position[servo]


                    # else:
                    #     print(Fore.YELLOW +f"Wrong value read from servo {servo}!")
                    #     break
                else:
                    print("Bus problem -- position")

            counter = counter + 1
            if counter >= 16:
                for servo in obj.servos:

                    tmp, ok = obj.read_temp(servo)
                    if ok:
                        obj.servo_temperature[servo] = tmp
                        if tmp >= self.max_temperature:
                            if obj.servo_temp_status[servo] == 'ok':
                                obj.servo_temp_status[servo] = 'ot'
                                print(Fore.RED + f"Servo {servo} is overheating {tmp}°C!")
                                obj.set_pwm_limit(servo, 0.02)     # limit the toque to 2%
                        elif tmp < obj.max_temperature-3:       # hysteresis 3°
                            if obj.servo_temp_status[servo] == 'ot':
                                obj.servo_temp_status[servo] = 'ok'
                                print(Fore.GREEN + f"Servo {servo} is back to notmal temp {tmp}°C")
                                obj.set_pwm_limit(servo, obj.servo_max_torque[servo])
                    else:
                        print(Fore.RED + "Bus problem -- temp read")

                counter = 0

            for servo in obj.servos:
                cur, ok = obj.read_current(servo)
                if ok:
                    obj.servo_current[servo] = cur
                else:
                    print(Fore.RED + "Bus problem -- current")

            # print(f"TIME {time.time()-ttt:.4F}")


        tl.start(block=False)
        self.do_polling = 'stop'         # all, stop, read

    def __del__(self):
        self.tl.stop()
        sleep(0.1)
        self.portHandler.closePort()

    # type - one of see the top of this file
    # name - any text name that we use to refer the servo
    # max_torque -  0..1
    # sets servo parameters and also status/error state
    # returns comm error, protocol error if any or 0, 0
    def add_servo(self, id, type, name, max_torque=0.2):
        self.servos[name] = id
        self.servo_types[id] = type
        self.servo_temp_status[name] = 'ok'

        # check if servo is alive and pre-fill position
        self.servo_position[name], alive = self.get_position_from_servo(name)
        if not alive:
            if DEBUG_PRINT:
                # print(f"Adding servo {id}..." + Fore.RED + f" Error: {self.packetHandler.getTxRxResult(com)}")
                print(f"Adding servo {id}..." + Fore.RED + f" Error")
        else:
            if DEBUG_PRINT:
                print(f"Adding servo {id}..." + Fore.GREEN + f" Success")
            # self.reset_servo(id)
            self.servo_max_torque[name] = max_torque
            self.set_pwm_limit(name, max_torque)
            # self.servo_old_position[name] = self.servo_position[name]

        return alive

    def get_type(self, name):
        id = self.servos[name]
        return self.servo_types[id]

    # read and normalises position to ±rads. Also returns "valid" flag
    def get_position_from_servo(self, name):
        id = self.servos[name]
        tp = self.servo_types[id]
        pos, com, err = self.read_position(name)
        self.servo_status[name] = com
        # if name == 'shoulder' and [True, False, False, False, False][random.randint(0,4)]:
        #     print(f"{pos}")
        if com == COMM_SUCCESS: # and err == 0:
            scale = (DXL_P180_POSITION_VALUE[tp] - DXL_N180_POSITION_VALUE[tp])
            norm_pos = (float(pos) - DXL_N180_POSITION_VALUE[tp]) / scale    # 0..1
            if norm_pos > 1:
                norm_pos = 1
            if norm_pos < 0:
                norm_pos = 0
            # 0..1 represents -2pi..2pi
            return (norm_pos - 0.5) * 2. * math.pi, True
        else:
            print(f"Position error {name}. {self.packetHandler.getTxRxResult(com)} {self.packetHandler.getRxPacketError(err)}")
            return 0.0, False

    def read_position(self, name):
        id = self.servos[name]
        type = self.servo_types[id]

        if (type == 'XL320'): #  uses 2 byte Position Data, Check the size of data in your DYNAMIXEL's control table
            dxl_present_position, dxl_comm_result, dxl_error = self.packetHandler.read2ByteTxRx(self.portHandler, id, ADDR_PRESENT_POSITION[type])
        else:
            dxl_present_position, dxl_comm_result, dxl_error = self.packetHandler.read4ByteTxRx(self.portHandler, id, ADDR_PRESENT_POSITION[type])
            if format(dxl_present_position, '032b')[0] == '1':
                dxl_present_position = dxl_present_position - POW_2_32

        return dxl_present_position, dxl_comm_result, dxl_error

    # blocking call that waits until finished
    def goto_position(self, name, pos, accuracy=0.001, stat=False):
        stat_pos = []
        stat_cur = []
        self.set_position(name, pos)
        while True:
            ppp, res = self.get_position('elbow_pitch')
            if stat:
                stat_pos.append(ppp)
                cur, res = self.read_current('elbow_pitch')
                stat_cur.append(cur)
            if abs(ppp - pos) < accuracy:     # [units: normalised to 1.0]
                break
            else:
                sleep(0.05)
            if not res:
                return False
        if stat:
            return stat_pos, stat_cur
        else:
            return True

    # send here -pi .. pi
    def set_position(self, name, pos):
        pos = pos / 2 / math.pi + 0.5       # to 0..1
        if pos > 1.0 or pos < 0.0:
            if DEBUG_PRINT:
                print(Fore.RED + f"Position must be [-pi..pi]! servo: {name} pos: {pos}")
            return False

        id = self.servos[name]
        type = self.servo_types[id]

        expanded_pos = pos * (DXL_P180_POSITION_VALUE[type] - DXL_N180_POSITION_VALUE[type]) + DXL_N180_POSITION_VALUE[type]
        expanded_pos = int(expanded_pos)

        # Write target position
        if type == 'XL320':  # XL320 uses 2 byte Position Data, Check the size of data in your DYNAMIXEL's control table
            dxl_comm_result, dxl_error = self.packetHandler.write2ByteTxRx(
                self.portHandler, id, ADDR_GOAL_POSITION[type], expanded_pos
            )
        else:
            dxl_comm_result, dxl_error = self.packetHandler.write4ByteTxRx(
                self.portHandler, id, ADDR_GOAL_POSITION[type],expanded_pos
            )

        if dxl_comm_result != COMM_SUCCESS:
            return False
        elif dxl_error != 0:
            return True

    def enable_torque(self, name, enable=False):
        id = self.servos[name]
        tp = self.servo_types[id]

        if enable:
            dxl_comm_result, dxl_error = self.packetHandler.write1ByteTxRx(self.portHandler, id, ADDR_TORQUE_ENABLE[tp], 1)
        else:
            dxl_comm_result, dxl_error = self.packetHandler.write1ByteTxRx(self.portHandler, id, ADDR_TORQUE_ENABLE[tp], 0)

        if dxl_comm_result != COMM_SUCCESS:
            if DEBUG_PRINT:
                if enable:
                    print(f"Cannot enable torque {id} {name} {self.packetHandler.getTxRxResult(dxl_comm_result)}")
                else:
                    print(f"Cannot disable torque {id} {name} {self.packetHandler.getTxRxResult(dxl_comm_result)}")

            return False

        return True

    def read_current(self, name):
        id = self.servos[name]
        type = self.servo_types[id]
        dxl_present_current, dxl_comm_result, dxl_error = \
            self.packetHandler.read2ByteTxRx(self.portHandler, id, ADDR_PRESENT_CURRENT[type])
        if dxl_comm_result == COMM_SUCCESS: # and dxl_error == 0:
            if format(dxl_present_current, '016b')[0] == '1':
                dxl_present_current = dxl_present_current - 65536 #pow(2, 16)
                current = float(dxl_present_current) * 2.69
            else:
                current = float(dxl_present_current) * 2.69
            return current, True
        else:
            return 0.0, False

    def read_temp(self, name):
        id = self.servos[name]
        type = self.servo_types[id]
        dxl_present_temp, dxl_comm_result, dxl_error =\
            self.packetHandler.read1ByteTxRx(self.portHandler, id, ADDR_PRESENT_TEMPERATURE[type])
        if dxl_comm_result == COMM_SUCCESS: # and dxl_error == 0:
            temperature = float(dxl_present_temp)
            return temperature, True
        else:
            return -273.6, False

    #  if you want to use Profiles, you need to set Profile Velocity(112) 52 to a non-zero value because setting Profile Velocity to 0 mean Profile not used
    # velo -- [0..1]
    def set_profile(self, name, max_velo, max_accel):
        id = self.servos[name]
        tp = self.servo_types[id]

        max_velo_expanded = int(max_velo * PROF_VELOCITY_MAX[tp])

        dxl_comm_result, dxl_error = self.packetHandler.write4ByteTxRx(self.portHandler, id, ADDR_VELOCITY_PROF[tp], max_velo_expanded)
        if dxl_comm_result != COMM_SUCCESS:
            return False

        max_accel_expanded = int(max_accel * PROF_ACCEL_MAX[tp])
        dxl_comm_result, dxl_error = self.packetHandler.write4ByteTxRx(self.portHandler, id, ADDR_ACCELERATION_PROF[tp], max_accel_expanded)
        if dxl_comm_result == COMM_SUCCESS:
            return True
        else:
            return False

    # we push the velocity to a "queue" that is dispatched by a sync thread. New value for a servo overwrites the old.
    # velocity [-1..1]
    def set_velocity(self, name, velo):
        id = self.servos[name]
        tp = self.servo_types[id]
        if velo > 1.:
            velo = 1.
        if velo < -1:
            velo = -1.

        self.desired_velocty[name] = int(velo * VELOCIY_MAX[tp])

        return True

    # stop all servos when in velocity mode
    def stop_velocity(self):
        for servo in self.servos:
            self.desired_velocty[servo] = 0
            # self.set_velocity(servo, 0)

    # mode == 0 -> current
    # mode == 1 -> velocity
    # mode == 3 -> position
    # mode == 16 -> pwm
    def set_mode(self, name, mode):
        id = self.servos[name]
        type = self.servo_types[id]
        old_polling = self.do_polling
        self.do_polling = 'stop'
        dxl_comm_result, dxl_error = self.packetHandler.write1ByteTxRx(self.portHandler, id, ADDR_OPER_MODE[type], mode)
        self.do_polling = old_polling
        if dxl_comm_result != COMM_SUCCESS:
            return False
        else:
            return True

    # 0..1
    def set_pwm_limit(self, name,  max_pwm):
        id = self.servos[name]
        type = self.servo_types[id]
        pwm_extended = int(PWM_MAX[type] * max_pwm)
        old_polling = self.do_polling
        self.do_polling = 'stop'
        dxl_comm_result, dxl_error = self.packetHandler.write2ByteTxRx(self.portHandler, id, ADDR_PWM_LIMIT[type], pwm_extended)
        self.do_polling = old_polling
        if dxl_comm_result == COMM_SUCCESS:
            return True
        else:
            return False

    def disable_protection(self, name):
        id = self.servos[name]
        type = self.servo_types[id]
        data = 0
        dxl_comm_result, dxl_error = self.packetHandler.write1ByteTxRx(self.portHandler, id, ADDR_SHUTDOWN[type], data)
        if dxl_comm_result == COMM_SUCCESS:
            return True
        else:
            return False

    def enable_protection(self, name):
        id = self.servos[name]
        tp = self.servo_types[id]
        data = 0x34
        dxl_comm_result, dxl_error = self.packetHandler.write1ByteTxRx(self.portHandler, id, ADDR_SHUTDOWN[tp], data)
        if dxl_comm_result == COMM_SUCCESS:
            return True
        else:
            return False

    # factory reset modes:
    # 0xFF : reset all values
    # 0x01 : reset all values except ID
    # 0x02 : reset all values except ID and baudrate
    def reset_servo(self, id, mode=0x02):
        # self.packetHandler.factoryReset(self.portHandler, id, mode)
        self.packetHandler.reboot(self.portHandler, id)


if __name__ == '__main__':
    bus = DynaBus("/dev/tty.usbserial-FT5NY874", 1000000)
    com, err = bus.add_servo(7, 'X_SERIES', 'gripper')

    while True:
        print(f"servo {bus.servo_position['gripper']}")
        sleep(1)
