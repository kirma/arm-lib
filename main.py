from time import sleep, time

import hid
import numpy as np
from colorama import Fore

from adr_arm import Arm
from dyna import DynaBus

deg = np.pi / 180.

# ======= joystick =======
# dt - timestep of IK (to calc speed control)
class Joy(object):
    def __init__(self, dt):
        for device in hid.enumerate():
            print(f"0x{device['vendor_id']:04x}, 0x{device['product_id']:04x} {device['product_string']}")
            if device['product_string'].find('Controller') >= 0:
                vendor_id = device['vendor_id']
                product_id = device['product_id']

        try:
            vendor_id
        except NameError:
            raise Exception("No controller found!")

        self.gamepad = hid.device()
        self.gamepad.open(vendor_id, product_id)
        self.gamepad.set_nonblocking(True)

        self.ctl_x, self.ctl_y, self.ctl_z = 0., 0., 0.
        self.ctl_twist = 0.0
        self.ctl_grip = 0.0
        self.ctl_r = 0.0
        self.dt = dt

    def read(self):
        channels = self.gamepad.read(23)
        if channels:
            # if the trigger pressed, we control the gripper
            if (channels[6] & 0b000100) != 0:
                self.ctl_grip = -100. / 255.
            elif (channels[6] & 0b001000) != 0:
                self.ctl_grip = 100. / 255.
            else:
                self.ctl_grip = 0.

            self.ctl_x = float(255 - channels[4] - 127) / 255.
            self.ctl_y = float(channels[3] - 127) / 255.
            self.ctl_z = float(255 - channels[2] - 127) / 255.
            self.ctl_twist = float(channels[1] - 127) / 255.
            self.ctl_r = float(channels[1] - 127) / 255.

            if abs(self.ctl_x) < 0.03:
                self.ctl_x = 0.
            if abs(self.ctl_y) < 0.03:
                self.ctl_y = 0.
            if abs(self.ctl_z) < 0.03:
                self.ctl_z = 0.
            if abs(self.ctl_twist) < 0.08:
                self.ctl_twist = 0.
            if abs(self.ctl_r) < 0.08:
                self.ctl_r = 0.

            # if (channels[5] & 0b100000) != 0:
            #     self.stop = True
            # else:
            #     self.stop = False


if __name__ == '__main__':

    dt_main = 0.06

    # ========= bus ==========
    my_bus = DynaBus("/dev/tty.usbserial-FT5NY874", 2000000, max_temp=60.0)

    my_bus.add_servo(43, 'PH54_SERIES', 'shoulder', max_torque=0.2)
    my_bus.add_servo(3, 'PH42_SERIES', 'elbow_pitch', max_torque=0.4)
    my_bus.add_servo(4, 'PH42_SERIES', 'elbow_yaw', max_torque=0.2)
    my_bus.add_servo(53, 'PH42_SERIES', 'wrist_yaw', max_torque=0.2)
    my_bus.add_servo(52, 'PH42_SERIES', 'wrist_pitch', max_torque=0.2)
    my_bus.add_servo(6, 'PH42_SERIES', 'wrist_roll', max_torque=0.2)
    my_bus.add_servo(7, 'X_SERIES', 'gripper', max_torque=0.8)

    # limiting the operation stage of the end effector [m]
    ee_stage = {
        'fw_start':  0.25,
        'fw_finish': 0.65,
        'side_start': -0.16,
        'side_finish': 0.16,
        'up_start': -.16,
        'up_finish': 0.43,
        'twist_range': np.pi/2.,   # +- pi
    }

    arm = Arm(my_bus, my_bus.servos, ee_stage, dt_main)


    # uncomment to drive servos manually and check the position read
    # my_bus.do_polling = True
    # for servo in my_bus.servos:
    #     print(servo+ "  ", end='')
    # print()
    # while True:
    #     for servo in my_bus.servos:
    #         tmp = my_bus.servo_position[servo]
    #         print(Fore.BLUE + f"{tmp:.3f}\t", end='')
    #     print()
    #     sleep(0.6)


    sleep(1)            # we must wait here! otherwise the servos screwed
    joy = Joy(arm.IK_PERIOD_MS)
    arm.home()
    arm.velocity_mode()
    sleep(0.8)
    # arm.calibrate_gripper()

    joy.read()
    # sleep(0.2)

    clock = -1
    while True:
        tm = time()
        joy.read()
        #
        # if joy_stop:
        #     arm.stop()
        #     print("Stopped!")
        # else:
        vel = np.array([joy.ctl_y, joy.ctl_x, joy.ctl_z])
        # print(joy.ctl_grip)
        if joy.ctl_r != 0.0:
            arm.ee_rot += joy.ctl_r / 76.
            # if joy.ctl_r > 0 and arm.arm_kinematics.ee_offset[0] < 0.1:
            #     arm.arm_kinematics.ee_offset[0] += joy.ctl_r / 50.
            # if joy.ctl_r < 0 and arm.arm_kinematics.ee_offset[0] < -0.1:
            #     arm.arm_kinematics.ee_offset[0] += joy.ctl_r / 50.
            # arm.offsets['wrist_yaw'] = joy.ctl_r / 10.
            vel[0] += joy.ctl_r


        arm.set_safe_ee_velocity(vel)
        # arm.set_ee_twist(joy.ctl_twist)

        # arm.ee_wrist_yaw = joy.ctl_twist / 8.
        # arm.ee_rot += joy.ctl_twist / 50.

        # TODO: Does not work!
        # TODO: check currents and temps!
        # if joy.ctl_r > 0 and arm.ee_rot < 0.4:
        #     arm.ee_rot  += joy.ctl_r / 50.
        # if joy.ctl_r <= 0 and arm.ee_rot > -0.4:
        #     arm.ee_rot  += joy.ctl_r / 50.


        arm.set_gripper_vel(joy.ctl_grip)

        # print(f"Vel: {vel[0]:3F} {vel[1]:3F} {vel[2]:3F}")
        if clock == -1:
            for servo in arm.bus.servos:
                print(Fore.BLUE + f"{servo}\t", end='')
            print()
        clock += 1
        if clock >= 100:
            clock = 0
            for servo in arm.bus.servos:
                tmp = arm.bus.servo_temperature[servo]
                if tmp > 40:
                    if tmp > 60:
                        print(Fore.RED + f"{tmp:.1f}\t\t", end='')
                    else:
                        print(Fore.YELLOW + f"{tmp:.1f}\t\t", end='')
                else:
                    print(Fore.GREEN + f"{tmp:.1f}\t\t", end='')
            print()
            print(arm.ee_pos)
            # print(arm.ee_wrist_yaw)

        # print(time() - tm)
        # arm.plot()
        # print(f"{my_bus.servo_position['elbow_yaw'] / deg} {my_bus.servo_position['wrist_yaw'] / deg}")

        dt = dt_main - (time() - tm)
        sleep(dt if dt > 0 else 0)
