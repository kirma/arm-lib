from time import sleep
import numpy as np
import time
# import ikpy.chain
# import ikpy.utils.plot as plot_utils
# import matplotlib.pyplot as plt
# visualise URDF: http://www.mymodelrobot.appspot.com/5674976526991360

import open3d as o3d
from kik.core import Actuator
from kik.optimizer import ScipyOptimizer
from kik.visualizer import build_geos

arm = Actuator(['z', [0.3, 0., 0.0], 'z', [0.064, 0., 0.], 'y', [0.4, 0., 0.]],
               bounds=((0,None), (None, -0.1), (0, None)))
# aaa = [np.pi*3/4, -np.pi*3/4, 0.0]
# arm.angles = aaa
endpoint = np.array([0.251, 0.212, 0])
st = time.time()
arm.ee = endpoint
print(f" Time: {(time.time() - st):4f}")
geos = build_geos(arm, target=arm.ee.tolist(), radius=0.02)

vis = o3d.visualization.Visualizer()
vis.create_window(width=640*2, height=480*2)

geos = build_geos(arm, target=arm.ee.tolist(), radius=0.02)
[vis.add_geometry(g) for g in geos]
vis.poll_events()
vis.update_renderer()

# [vis.add_geometry(g) for g in geos]
# vis.poll_events()
# vis.update_renderer()
# vis.add_geometry(o3d.visualization.draw_geometries(geos))
for i in range(15):
    print(f"Pos: {arm.ee.tolist()}, Ang: {arm.angles}")
    vis.clear_geometries()
    geos = build_geos(arm, target=arm.ee.tolist(), radius=0.02)
    [vis.add_geometry(g) for g in geos]
    vis.poll_events()
    # vis.update_renderer()

    av = arm.get_ang_vel(np.array([0.3, 0, 0]), dt=0.07)

    endpoint = endpoint + np.array([+0.02, 0, 0])
    arm.ee = endpoint
    sleep(0.05)
# tinyik.visualize(arm, animate, target=arm.ee.tolist())

