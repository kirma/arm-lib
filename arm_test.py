from time import sleep
import numpy as np
import math
import hid      # pip install hidapi
from dyna import DynaBus
from kik import Actuator
from path_planning import servo_quintic_planner

precision_end = 0.1
precision_start = 0.2
ee_control_precision = 4

stage_x_start = 0.2
stage_y_start = 0.0    # ??????? TODO: fix this
stage_x_finish = 0.5
stage_z_start = -.2
stage_z_finish = 0.2

dt = 0.1           # time tick [s]
max_accel = 1.0     # max accel [/ss]
max_jerk = 5.0      # max jerk [/sss]
tm_min = 0.9        # search space for full trajectory (sec)
tm_max = 6.0        # search space for full trajectory (sec)

# locate which viapoint we're on
# idx_min: our last viapoint
# retuns the idx of the bin or -1 if not found
def locate(bins, idx_min, val):
    for i in range (idx_min, len(bins)-1):
        if lays_in(bins, i, val):
            return i

    # if we close to the end, we still OK
    if abs(bins[-1] - val) < precision_end:
        return len(bins)

    # we have not located the bin in trajectrory that include our current pos
    return -1


def lays_in(bins, idx, val):
    if bins[idx] == bins[idx+1] and bins[idx] == val:
        return True

    if bins[idx] > bins[idx+1]:
        # decreasing sequence
        if val <= bins[idx] and val > bins[idx+1]:
            return True
        else:
            return False
    else:
        # increasing sequence
        if val >= bins[idx] and val < bins[idx+1]:
            return True
        else:
            return False


directions = {}
directions['elbow_pitch'] = -1.
directions['shoulder'] = 1.

offsets = {}
offsets['elbow_pitch'] = 0
offsets['shoulder'] = math.pi / 2.


def get_ee_position():
    ang0, res = bus.get_position('shoulder')
    ang1, res = bus.get_position('elbow_pitch')
    arm.angles = np.array([
        ang0 * directions['shoulder'] + offsets['shoulder'],
        ang1 * directions['elbow_pitch'] + offsets['elbow_pitch'],
        0.0
        ])
    pos3d = arm.ee
    # print(f"Pos: {pos3d[0]:4F} {pos3d[1]:4F} Ang: {ang0:4F} {ang1:4F} Servo Ang: {arm.angles[0]:4F} {arm.angles[1]:4F}")
    return pos3d


def set_servo_velocities(ee_velocity):
    vvv_ang = arm.get_ang_vel(ee_velocity)
    bus.set_velocity('shoulder', vvv_ang[0] * directions['shoulder'])
    bus.set_velocity('elbow_pitch', vvv_ang[1] * directions['elbow_pitch'])


if __name__ == "__main__":
    for device in hid.enumerate():
        print(f"0x{device['vendor_id']:04x}, 0x{device['product_id']:04x} {device['product_string']}")
        if device['product_string'].find('Controller') >= 0:
            vendor_id = device['vendor_id']
            product_id = device['product_id']

    if not vendor_id:
        raise Exception("No controller found!")

    gamepad = hid.device()
    gamepad.open(vendor_id, product_id)
    gamepad.set_nonblocking(True)


    arm = Actuator(['z', [0.3, 0., 0.0], 'z', [0.064, 0., 0.], 'y', [0.4, 0., 0.]],
                   bounds=((0, None), (None, -0.1), (0, None)))

    bus = DynaBus("/dev/tty.usbserial-FT5NY874", 1000000)

    bus.add_servo(3, 'PH42_SERIES', 'elbow_pitch')
    bus.add_servo(4, 'PH42_SERIES', 'elbow_yaw')
    bus.add_servo(5, 'X_SERIES', 'wrist_yaw')
    bus.add_servo(6, 'PH42_SERIES', 'wrist_roll')
    bus.add_servo(7, 'X_SERIES', 'gripper')
    bus.add_servo(43, 'PH54_SERIES', 'shoulder')

    home_pos = {}
    home_pos['elbow_pitch'] = 2.5
    home_pos['elbow_yaw'] = 0.0
    home_pos['wrist_yaw'] = 0.0
    home_pos['wrist_roll'] = 0.0
    home_pos['gripper'] = -0.2
    home_pos['shoulder'] = 1.0


    # go home
    for servo in bus.servos:
        bus.enable_torque(servo, False)
        bus.set_mode(servo, 3)
        bus.set_profile(servo, 1.0, 0.2)
        bus.set_pwm_limit(servo, 0.2)
        bus.enable_torque(servo, True)
        bus.set_position(servo, home_pos[servo])
    while True:
        if abs(bus.get_position('shoulder')[0] - home_pos['shoulder']) < precision_start:
            break
        else:
            sleep(0.3)

    # switch to velocity mode AND disable fng protection
    for servo in bus.servos:
        bus.enable_torque(servo, False)
        bus.set_mode(servo, 1)
        bus.disable_protection(servo)
        bus.enable_torque(servo, True)

    traj_idx = -1

    # empty trajectories for beginning
    x = np.ones(5) * stage_x_start
    v = np.zeros(5)

    x_desired = 127
    z_desired = 127
    x_desired_old = x_desired
    z_desired_old = z_desired

    # pos3d_old = np.array([stage_x_start, stage_y_start, stage_z_start])
    pos3d = get_ee_position()
    pos_x = pos3d[0]
    vel_x = 0.0
    flag = True
    while True:
        channels = gamepad.read(64)
        if flag and channels:
            # if abs(x_desired_old - float(255-channels[4])) > ee_control_precision:
            x_desired_old = x_desired
            z_desired_old = z_desired
            x_desired = float(255-channels[4])
            z_desired = float(channels[3])
            x_des_ee = x_desired/255. * (stage_x_finish - stage_x_start) + stage_x_start
            z_des_ee = 0.0  # TODO: fix these
            _, x, v, a, j = servo_quintic_planner(pos_x, vel_x, 0, x_des_ee, 0, 0, max_accel, max_jerk, tm_min, tm_max, dt)
            traj_idx = -1
            # print(f"Control: {x_des_ee} Real: {pos_x} {vel_x} ")
            print(f"Real: {pos_x} {vel_x} Tra: {v[0]} ")
            # flag = False

        do_sleep = False

        pos3d_old = pos3d
        pos3d = get_ee_position()
        pos_x = pos3d[0]

        vel_x = (pos_x - pos3d_old[0]) / dt
        # print(f"Real: {pos_x} Expected: {xxx[bin]}")

        # if we just started, we could be outside of the first bin. So, pretend we're there IF we're close to
        if traj_idx == -1:
            bin = locate(x, 0, pos_x)

            if bin == -1:
                # we 're close enough to the starting position
                if abs(pos_x - x[0]) < precision_start:
                    set_servo_velocities(np.array([v[0], 0., 0.]))
                    do_sleep = True
                    continue
                else:
                    bus.stop_velocity()
                    do_sleep = True
                    print("Incorrect starting position - out of trajectory")
                    break
            elif bin == len(x):
                # we passed the end point
                print(f'Trajectory ended with overshoot, req: {x[-1]} real: {pos_x:3F}')
                flag = True
                bus.stop_velocity()
            else:
                traj_idx += 1
                continue
        else:
            bin = locate(x, 0, pos_x)
            # if traj_idx == 0:
            #     bin = locate(x, traj_idx, pos_x)
            # else:
            #     bin = locate(x, traj_idx-1, pos_x)      # start from a 2nd prev bin, because of noise

            if bin >= len(x) - 2:
                bus.stop_velocity()
                flag = True
                print(f'Trajectory ended, pos: {pos_x:3F}')
                do_sleep = True
                # break
            elif bin < 0:
                flag = True
                print(f'Out of trajectory: trj {x[traj_idx]:3F}, pos {pos_x:3F}, idx {traj_idx}')
                do_sleep = True
                bus.stop_velocity()
                # break
            else:
                # within an interval
                traj_idx = bin
                set_servo_velocities(np.array([v[traj_idx], 0., 0.]))

            do_sleep = True

        # if do_sleep:
        sleep(dt / 2)

        # current_pos = get_ee_position()
        # current_vel = v[traj_idx]
        # if channels:
        #     print(f"{channels[3]}  {channels[4]}")

        # sleep(1)