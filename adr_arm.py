from datetime import timedelta
from random import random, randint
from time import sleep, clock_gettime, CLOCK_PROCESS_CPUTIME_ID, time
import numpy as np
from roboticstoolbox import ERobot, Link, tools
from roboticstoolbox.robot import ET
from spatialmath import SE3, SO3

from dyna import DynaBus
from ktl import SuperTimeloop

max_gripper_current = 120   # mA
ik_period_ms = 33.

deg = np.pi / 180.

# home - home position  angles in kimenatics
class ArmKinematics(ERobot):
    def __init__(self, home, dt):
        # the qlim implemented in Link() does not fkng do anything. But we can check these manually with self.islim()
        l0 = Link(ET.tz(0.010) * ET.Rx(), name="base", parent=None, qlim=(-60.*deg, 60.*deg))
        l1 = Link(ET.tz(0.223) * ET.Rx(), name="link1", parent=l0, qlim=(-150.*deg, 0.*deg))
        l2 = Link(ET.tz(0.060) * ET.Ry(), name="link2", parent=l1, qlim=[-60.*deg, 60.*deg])
        l3 = Link(ET.tz(0.174) * ET.Ry(), name="link3", parent=l2, qlim=[-60.*deg, 60.*deg], joint_name="wrist_yaw")
        l4 = Link(ET.tz(0.060) * ET.Rx(), name="link4", parent=l3, qlim=[-80.*deg, 45.*deg])
        l5 = Link(ET.tz(0.020) * ET.Rz(), name="link5", parent=l4)
        ee = Link(ET.tz(0.200), name="ee", parent=l5)
        elinks = [l0, l1, l2, l3, l4, l5, ee]

        super(ArmKinematics, self).__init__(elinks, name="ADR Arm", manufacturer="ADR")

        self.dt = dt
        T = self.fkine(home)
        # pos = T.t
        # self.ee_old_pos = np.array([pos[2], pos[0], pos[1]])    # re-arrange to our representatin vs ik
        self.ee_old_pos = T.t
        print(self.ee_old_pos)
        # rotation of the ee to get the initial straight position
        self.R = np.array([
            [ 1.,  0.,  0.],
            [ 0.,  0.,  1.],
            [ 0., -1.,  0.]])
        # current rotation along the vert axis
        self.ee_rot_now = 0.
        self.ee_offset = np.array([0.0, 0.0, 0.0])  # we don't use this

        # init with something. we only use this as a starting point to speedup the optimiser convergence
        # self.old_joint_sol = self.ikine_LM(T)
        self.real_arm_q = self.ikine_LM(T).q

        # self.ik_wrist_yaw = 0.0

    # take into account the desired ee orientation
    # ee_rot is the rotation of the ee along the vertical axis (left/right)
    def solve_ik(self, ee_pos, ee_rot):
        return self.ikine_LM(SE3(ee_pos) * SE3(SO3(self.R)) * SE3(SO3().Ry(ee_rot)), tol=1e-2)

    def get_ang_vel_from_ee(self, ee_velo, ee_pos_now, ee_rot, q_now): # ee_rot excluded
        ee_new_pos = ee_pos_now + ee_velo * self.dt
        ttt = time()

        # the coords in ik sidewize, forward, up

        residual = 0.01
        new_joints_sol = self.ikine_LM(
            SE3(ee_new_pos + self.ee_offset) * SE3(SO3(self.R)) * SE3(SO3().Ry(ee_rot)),
            q0=q_now, tol=residual, ilimit=300)
        # SE3(ee_new_pos) * SE3().Rt(self.R) * SE3().Ry(ee_rot)

        # new_joints_sol = self.ikine_LMS(SE3(ee_new_pos + self.ee_offset) * SE3(SO3(self.R)) * SE3(SO3().Ry(ee_rot)), tol=residual)
        # new_joints_sol = self.ikine_min(
        #     SE3(ee_new_pos + self.ee_offset) * SE3(SO3(self.R)) * SE3(SO3().Ry(ee_rot)),
        #     q0=self.real_arm_q, tol=residual, method='Nelder-Mead')

        # if no solution, try re-run with less precise tolerance
        # if not new_joints_sol.success:
        #     residual = 0.01
        #     new_joints_sol = self.ikine_LM(
        #         SE3(ee_new_pos + self.ee_offset) * SE3(SO3(self.R)) * SE3(SO3().Ry(ee_rot)),
        #         q0=self.real_arm_q, tol=residual)

        # joints_now_sol = self.ikine_LMS(SE3(ee_pos_now) * SE3(SO3(self.R)) * SE3(SO3().Ry(self.ee_rot_now)), tol=residual)
        joints_now_sol = self.ikine_LM(
            SE3(ee_pos_now) * SE3(SO3(self.R)) * SE3(SO3().Ry(self.ee_rot_now)),
            q0=q_now, tol=residual)

        # joints_now_sol = self.ikine_min(
        #     SE3(ee_pos_now) * SE3(SO3(self.R)) * SE3(SO3().Ry(self.ee_rot_now)),
        #     q0=self.real_arm_q, tol=residual, method='Nelder-Mead')

        dist = np.linalg.norm(joints_now_sol.q - q_now)
        ttt = time()-ttt   # <5ms
        if randint(0, 100) < 6:
            print(f"Opt Time {ttt:.4F} \tDistance {dist:.3F} ")
            if dist > 0.1:
                print(f"IK   {joints_now_sol.q}")
                print(f"Real {q_now}")

        if new_joints_sol.success and joints_now_sol.success and dist < 0.8:
            # adjusted_q =  q_now
            adjusted_q = joints_now_sol.q * 0.3 + 0.7 * q_now
            # adjusted_q = joints_now_sol.q
            # self.ik_wrist_yaw = new_joints_sol.q[3]    # we use this position instead of the real one because the control is partially decoupled from IK
            qt = tools.trajectory.jtraj(adjusted_q, new_joints_sol.q, 3)
            self.real_arm_q = adjusted_q
            self.ee_rot_now = ee_rot
            q_vels = qt.qd[1]

            # if the real servo positions diverge from the IK, we need to catch up
            delta = joints_now_sol.q - q_now
            q_vels = q_vels + delta * 0.01

        else:
            q_vels = np.zeros(self.n)
            print(f"No solution, residual: {new_joints_sol.residual:.5f}  {ee_new_pos}")


        return q_vels


# we pass pre-initialised dyna bus with all joints defined. We only support one configuration at the moment
# bus - an initiated dyna bus object with all servos populated
# stage - the operation stage of the end effector, metres
class Arm(object):
    def __init__(self, dyna_bus, servos, stage, dt=None):
        self.bus = dyna_bus
        self.stage = stage
        self.IK_PERIOD_MS = ik_period_ms

        # this going to be the home position
        self.home_pos = {
            'shoulder': 55.*deg,
            'elbow_pitch': 140.*deg,
            'elbow_yaw': 0.0,
            'wrist_yaw': 0.0,
            'wrist_roll': 0.0,
            'wrist_pitch': 0.0,
            # 'gripper': -0.2,
        }

        # how accurate we want to be to reach the home position
        self.home_accuracy = 0.05
        self.servos = servos

        # the mistmatch between IK model motors and the real arm servos
        self.directions = {
            'shoulder': 1.,
            'elbow_pitch': -1.,
            'elbow_yaw': 1.,
            'wrist_yaw': 1.,
            'wrist_pitch': -1.,
            'wrist_roll': 1.,
        }
        self.offsets = {
            'shoulder': 0.,
            'elbow_pitch': 0.,
            'elbow_yaw': 0.,
            'wrist_yaw': 0.,
            'wrist_pitch': 0.,
            'wrist_roll': 0.,
        }
        # must be the same!
        vel_scale = 0.25
        self.velocity_scale = {
            'shoulder': vel_scale,
            'elbow_yaw': vel_scale,
            'elbow_pitch': vel_scale,
            'wrist_roll': vel_scale,
            'gripper': 0.3,
            'wrist_yaw': vel_scale,
            'wrist_pitch': vel_scale,
        }

        home_servos = np.array([
            self.home_pos['shoulder'] * self.directions['shoulder'] + self.offsets['shoulder'],
            self.home_pos['elbow_pitch'] * self.directions['elbow_pitch'] + self.offsets['elbow_pitch'],
            self.home_pos['elbow_yaw'] * self.directions['elbow_yaw'] + self.offsets['elbow_yaw'],
            self.home_pos['wrist_yaw'] * self.directions['wrist_yaw'] + self.offsets['wrist_yaw'],
            self.home_pos['wrist_pitch'] * self.directions['wrist_pitch'] + self.offsets['wrist_pitch'],
            self.home_pos['wrist_roll'] * self.directions['wrist_roll'] + self.offsets['wrist_roll'],
        ])

        self.arm_kinematics = ArmKinematics(home=home_servos,
                                            dt=dt
                                            )

        # pre-init positions
        self.ee_vel = np.array([0.,0.,0.])
        self.ee_pos = np.array([0.,0.,0.])
        self.ee_twist = 0.0
        self.gripper_zero = 0.5     # in 0..1
        self.ee_rot = 0.0           # wrist yaw controlled by IK. We pretend it always 0.0
        # self.ee_wrist_yaw = 0.0     # this joint is partially controlled in isolation of IK and this is the divergence

        # self.wrist_yaw_offset = 0.0
        # self.elbow_yaw = 0.0        # fake servo position that is linked to IK

        # old_pos = self.ee_pos
        # self.ee_pos = self.get_ee_positions()
        # self.ee_vel = (self.ee_pos - old_pos) / self.IK_PERIOD_MS / 1000.

        # Timeloop object allows to periodically call a procedure with high precision and protection
        tl = SuperTimeloop()
        self.tl = tl

        @tl.job(timedelta(milliseconds=self.IK_PERIOD_MS), self)
        def polling_servo_job(self):
            try:
                # we only use ee_pos for kinematics. The others used for checking ranges
                old_pos = self.ee_pos
                self.ee_pos = self.ee_pos * 0.5 + 0.5 * self.get_ee_positions()
                self.ee_vel = self.ee_vel * 0.5 + 0.5 * (self.ee_pos - old_pos) / self.IK_PERIOD_MS / 1000.
                self.ee_twist = self.get_twist()
                # self.arm_kinematics.real_arm_q = self.arm_kinematics.real_arm_q * 0.95 + 0.05 * self.get_q

                # TODO: hard stop - if servos are over limits


            except:
                print("Cannot catch up!")
                pass

        tl.start(block=False)

    def __del__(self):
        self.tl.stop()
        sleep(0.1)

    def plot(self):
        self.arm_kinematics.plot(self.bus.get_position, jointaxes=True, limits=np.array([-0.4, 0.4, -0.4, 0.4, 0, 0.4]))

    # returns -pi .. pi
    def get_twist(self):
        tw = self.bus.servo_position['wrist_roll']  # normlised to -pi .. pi
        # print(tw)
        return tw

    # switch to "velocity" control mode AND disable fng protection
    def velocity_mode(self):
        self.bus.do_polling = 'stop'
        sleep(0.1)
        for servo in self.servos:
            self.bus.enable_torque(servo, False)
            self.bus.set_mode(servo, 1)
            self.bus.disable_protection(servo)
            self.bus.enable_torque(servo, True)
        self.bus.do_polling = 'all'

    # === go home ===
    # blocking function to home the arm. Also, it switches the mode to "position" control
    # does not home gripper
    def home(self):
        timeout = 12.0  # sec
        self.bus.do_polling = 'stop'
        for servo in self.servos:
            self.bus.enable_torque(servo, False)
            self.bus.set_mode(servo, 3)
            # sleep(0.1)
            # self.bus.set_profile(servo, 0.5, 0.1)
            if servo == 'elbow_pitch':
                self.bus.set_pwm_limit(servo, 0.3)
                self.bus.set_profile(servo, 0.5, 0.1)
            else:
                self.bus.set_pwm_limit(servo, 0.2)
                self.bus.set_profile(servo, 1.0, 0.1)
            # if self.bus.get_type(servo) == "PH54_SERIES":
            #     # these servos are slower
            #     self.bus.set_profile(servo, 1.0, 0.1)
            # else:
            #     self.bus.set_profile(servo, 0.8, 0.1)
            # self.bus.set_pwm_limit(servo, 0.2)
            # sleep(0.1)

            if self.bus.enable_torque(servo, True):
                # do not home gripper
                if servo != 'gripper':
                    self.bus.set_position(servo, self.home_pos[servo])
            else:
                raise Exception(f"Cannot initialise servo {servo}")

        self.bus.do_polling = 'stop'
        sleep(0.2)      # let it fill all positions
        while True:
            flag = True
            for servo in ['shoulder','elbow_pitch', 'elbow_yaw', 'wrist_yaw', 'wrist_pitch']:
                # if abs(self.bus.servo_position[servo] - self.home_pos['shoulder']) > self.home_accuracy:
                pos, ok = self.bus.get_position_from_servo(servo)
                if not ok or abs(pos - self.home_pos[servo]) > self.home_accuracy:
                    flag = False
                    break
            if flag:
                break
            else:
                sleep(0.25)
                timeout -= 0.25
                if timeout < 0:
                    # for servo in ['shoulder', 'elbow_pitch', 'elbow_yaw', 'wrist_yaw']:
                    #     if (ddd := abs(self.bus.servo_position[servo] - self.home_pos['shoulder'])) > self.home_accuracy:
                    #         print(f'{servo} not ready {ddd:.3F}')
                    raise Exception(f'Go Home failed')

        self.bus.do_polling = 'all'

    # call this only after switching to the velocity control mode!
    def calibrate_gripper(self):
        self.bus.set_pwm_limit('gripper', 0.05)
        t = 0
        self.bus.set_velocity('gripper', 0.05)
        # keep closing until the current reach 100mA or 5 sec
        while self.bus.servo_current['gripper'] < max_gripper_current and t < 5:
            t = t + 0.2
            sleep(0.2)
            # print(f"{self.bus.servo_current['gripper']}")

        self.bus.set_velocity('gripper', 0.0)
        self.gripper_zero = self.bus.servo_position['gripper']
        # wait till the command is execured by dispatcher
        sleep(0.2)
        # back to requested torque
        self.bus.set_pwm_limit('gripper', self.bus.servo_max_torque['gripper'])


    # convert real servo positions to kenematics required (flip + ofset)
    # servo_pos: array of 3 servo positions
    def real_to_kinematics(self, servo_pos):
        angles = np.array([
            servo_pos[0] * self.directions['shoulder'] - self.offsets['shoulder'],
            servo_pos[1] * self.directions['elbow_pitch'] - self.offsets['elbow_pitch'],
            servo_pos[2] * self.directions['elbow_yaw'] - self.offsets['elbow_yaw'],
            servo_pos[3] * self.directions['wrist_yaw'] - self.offsets['wrist_yaw'],
            servo_pos[4] * self.directions['wrist_pitch'] - self.offsets['wrist_pitch'],
            servo_pos[5] * self.directions['wrist_roll'] - self.offsets['wrist_roll'],
        ])
        return angles

    # get a vector with joint positions converted to IK frames
    def get_q(self):
        servo_angles = np.array([
            self.bus.servo_position['shoulder'],
            self.bus.servo_position['elbow_pitch'],
            self.bus.servo_position['elbow_yaw'],
            self.bus.servo_position['wrist_yaw'],
            # self.arm_kinematics.old_joint_sol.q[3],
            self.bus.servo_position['wrist_pitch'],
            self.bus.servo_position['wrist_roll'],
        ])
        ik_angles = self.real_to_kinematics(servo_angles)
        return ik_angles

    # read the servos and calc end effector position using IK. Does not tale into account the real position of wrist_yaw
    def get_ee_positions(self):
        ik_angles = self.get_q()        # converted to IK frame
        pos3d_sol = self.arm_kinematics.fkine(ik_angles)
        pos = pos3d_sol.t
        # print(f"Pos: {pos[0]:3F} {pos[1]:3F} {pos[2]:3F} Servo: {ik_angles:3F} {ik_angles:3F} {ik_angles:3F}")
        return pos

    # respect the stage borders taking into account our intended motion direction.
    # We allow to move back inside the stage, but not outside
    # returns True if OK, False if not (also stops all servos)
    def ee_inside_stage(self, desired_velo):
        if self.ee_pos[1] <= self.stage['fw_start']:
            if desired_velo[1] < 0 or self.ee_vel[1]+0.0001 < 0:
                self.stop()
                # print(f"stopped 2! {pos}")
                return False
        if self.ee_pos[1] >= self.stage['fw_finish']:
            if desired_velo[1] > 0 or self.ee_vel[1]-0.0001 > 0:
                self.stop()
                # print(f"stopped 3! {pos}")
                return False
        if self.ee_pos[2] <= self.stage['up_start']:
            if desired_velo[2] < 0 or self.ee_vel[2]+0.0001 < 0:
                self.stop()
                # print(f"stopped 2! {pos}")
                return False
        if self.ee_pos[2] >= self.stage['up_finish']:
            if desired_velo[2] > 0 or self.ee_vel[2]-0.0001 > 0:
                self.stop()
                # print(f"stopped 3! {pos}")
                return False
        if self.ee_pos[0] <= self.stage['side_start']:
            if desired_velo[0] < 0 or self.ee_vel[0]+0.0001 < 0:
                self.stop()
                # print(f"stopped 2! {pos}")
                return False
        if self.ee_pos[0] >= self.stage['side_finish']:
            if desired_velo[0] > 0 or self.ee_vel[0]-0.0001 > 0:
                self.stop()
                # print(f"stopped 3! {pos}")
                return False

        return True

    # checks if the joints are within the safe range and sends velocities to the servos given the end effector velocity
    def set_ee_velocity(self, ee_velocity):
        ikq = self.get_q()
        vel_q = self.arm_kinematics.get_ang_vel_from_ee(ee_velocity, self.ee_pos, self.ee_rot, ikq)

        # check the joints' limits
        for i in range(len(ikq)):
            try:    # we need to check if the limits were set in IK. If not, we got exception
                if ikq[i] < self.arm_kinematics.links[i].qlim[0] and vel_q[i] < 0.:
                    self.stop()
                    return
                if ikq[i] > self.arm_kinematics.links[i].qlim[1] and vel_q[i] > 0.:
                    self.stop()
                    return
            except:
                pass

        # added the extra control (velocity) along with IK
        # if not (self.ee_wrist_yaw > 0 and self.bus.servo_position['wrist_yaw'] > self.arm_kinematics.links[3].qlim[1]):
        #     vel_q[3] += self.ee_wrist_yaw
        # elif not (self.ee_wrist_yaw < 0 and self.bus.servo_position['wrist_yaw'] < self.arm_kinematics.links[3].qlim[0]):
        #     vel_q[3] += self.ee_wrist_yaw

        # seems safe to carry on
        self.bus.set_velocity('shoulder',    vel_q[0] * self.directions['shoulder']    * self.velocity_scale['shoulder'])
        self.bus.set_velocity('elbow_pitch', vel_q[1] * self.directions['elbow_pitch'] * self.velocity_scale['elbow_pitch'])
        self.bus.set_velocity('elbow_yaw',   vel_q[2] * self.directions['elbow_yaw']   * self.velocity_scale['elbow_yaw'])
        self.bus.set_velocity('wrist_yaw',   vel_q[3] * self.directions['wrist_yaw']   * self.velocity_scale['wrist_yaw'])
        self.bus.set_velocity('wrist_pitch', vel_q[4] * self.directions['wrist_pitch'] * self.velocity_scale['wrist_pitch'])
        self.bus.set_velocity('wrist_roll',  vel_q[5] * self.directions['wrist_roll']  * self.velocity_scale['wrist_roll'])

    # sends velocities [-1..1] to the servos given the end effector velocity checking the stage
    def set_safe_ee_velocity(self, ee_velocity):
        if self.ee_inside_stage(ee_velocity):
            self.set_ee_velocity(ee_velocity)

    def stop(self):
        for servo in self.servos:
            self.bus.set_velocity(servo, 0.)
        sleep(self.IK_PERIOD_MS/1000.*5)

    # -1..1
    def set_ee_twist(self, ee_twist_velocity):
        # print(ee_twist_velocity)
        if self.ee_twist < self.stage['twist_range']:
            if ee_twist_velocity >= 0:
                self.bus.set_velocity('wrist_roll', ee_twist_velocity * self.velocity_scale['wrist_roll'])
                return True
        if self.ee_twist > -self.stage['twist_range']:
            if ee_twist_velocity <= 0:
                self.bus.set_velocity('wrist_roll', ee_twist_velocity * self.velocity_scale['wrist_roll'])
                return True

        self.bus.set_velocity('wrist_roll', 0)
        return False

    def set_gripper_vel(self, vel):

        if vel == 0:
            self.bus.set_velocity('gripper', 0)
            return

        if vel < 0 and self.bus.servo_position['gripper'] < (self.gripper_zero - 1./13.):   # too wide
            self.bus.set_velocity('gripper', 0)
            # print('too wide')
            return

        if vel > 0 and self.bus.servo_position['gripper'] >= (self.gripper_zero+0.2):   # closed
            self.bus.set_velocity('gripper', 0)
            self.gripper_zero = self.bus.servo_position['gripper']
            # print('overclosed')
            return

        if abs(self.bus.servo_current['gripper']) < max_gripper_current:
            self.bus.set_velocity('gripper', vel * self.velocity_scale['gripper'])
        else:
            # relax gripper
            # self.bus.do_polling = False
            self.bus.set_pwm_limit('gripper', 0.05)
            if vel > 0:
                self.gripper_zero = self.bus.servo_position['gripper']
            sleep(0.1)
            self.bus.set_pwm_limit('gripper', self.bus.servo_max_torque['gripper'])
            self.bus.set_velocity('gripper', 0)
            # self.bus.do_polling = True
            # print(f"Over current {self.bus.servo_current['gripper']}")


if __name__ == '__main__':

    my_bus = DynaBus("/dev/tty.usbserial-FT5NY874", 1000000)
    my_bus.add_servo(3, 'PH42_SERIES', 'elbow_pitch')
    my_bus.add_servo(4, 'PH42_SERIES', 'elbow_yaw')
    my_bus.add_servo(5, 'X_SERIES', 'wrist_yaw')
    my_bus.add_servo(6, 'PH42_SERIES', 'wrist_roll')
    my_bus.add_servo(7, 'X_SERIES', 'gripper')
    my_bus.add_servo(43, 'PH54_SERIES', 'shoulder')

    # limiting the operation stage of the end effector [m]
    ee_stage = {
        'fw_start':  0.25,
        'fw_finish': 0.65,
        'side_start': -0.2,
        'side_finish': 0.2,
        'up_start': -.1,
        'up_finish': 0.44
    }

    arm = Arm(my_bus, my_bus.servos, ee_stage)
    arm.home()
    arm.velocity_mode()
    sleep(1)

    vel = np.array([0.0, 0.0, 0.2])
    # vel_q = [-0.02750979,  0.10219495,  0., -0.07470749,  0.]
    # [-0.06739378  0.12400733  0. - 0.05641278  0.]
    # [-0.00959877  0.08885949  0. - 0.07927801  0.]
    # [-0.01898444  0.09573845  0. - 0.07677405  0.]
    # [-0.02750979  0.10219495  0. - 0.07470749  0.]
    # [-0.03529738  0.10833916  0. - 0.07306603  0.]
    # [-0.04248055  0.11428827  0. - 0.07183366  0.]
    # [-0.04919788  0.12016569  0. - 0.07099534  0.]
    # [-0.05559071  0.1261025   0. - 0.07054088  0.]

    while True:
        arm.set_safe_ee_velocity(vel)
        # arm.bus.set_velocity('shoulder', vel_q[0] * arm.directions['shoulder'] * arm.velocity_scale['shoulder'])
        # arm.bus.set_velocity('elbow_pitch', vel_q[1] * arm.directions['elbow_pitch'] * arm.velocity_scale['elbow_pitch'])
        # arm.bus.set_velocity('elbow_yaw', vel_q[2] * arm.directions['elbow_yaw'] * arm.velocity_scale['elbow_yaw'])
        # print(arm.get_ee_positions())

        # print(f"Pos: {arm.ee_pos[0]:.3F} {arm.ee_pos[1]:.3F} {arm.ee_pos[2]:.3F} ServoAng: {my_bus.servo_position['shoulder']:3F} {my_bus.servo_position['elbow_pitch']:3F} ")
        print(f"Pos: {arm.ee_pos[0]:.3f} {arm.ee_pos[1]:.3f} {arm.ee_pos[2]:.3f} vel: {arm.ee_vel[0]:.3f} \t{arm.ee_vel[1]:.3f}  \t{arm.ee_vel[2]:.3f}")
        if arm.ee_pos[2] >= 0.45 or arm.ee_pos[0] >= 0.25:
            print("Out!")
            arm.stop()
            break
        sleep(0.05)

    arm.stop()
    # arm.stop()

