import math
import time
from time import sleep
from DynamixelSDK.python.src.dynamixel_sdk import COMM_SUCCESS
from dyna import DynaBus, PROF_ACCEL_MAX, VELOCITY_MAX_RADS
import numpy as np
import matplotlib.pyplot as plt

from kik import Actuator
from path_planning import servo_quintic_planner

'''
Velocity control with polynomial path planning
'''

def gaussian(x, mu, sig):
    return np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.)))

# returns:
# 0 -> in the bin
# 1 -> in the next bin
# -1 -> out of bin/next bin or the second last via point
def on_track(bins, idx, val):
    if lays_in(bins, idx, val):
        return 0
    else:
        if idx < len(bins)-2:
            # check if we're not in the second last bin
            if lays_in(bins, idx+1, val):
                return 1
            else:
                return -1
        else:
            if bins[idx] > bins[idx+1]:
                if val < bins[idx+1]:
                    return 1
                else:
                    return -2
            else:
                if val > bins[idx+1]:
                    return 1
                else:
                    return -2


# locate which viapoint we're on
# idx_min: our last viapoint
# retuns the idx of the bin or -1 if not found
def locate(bins, idx_min, val):

    for i in range (idx_min, len(bins)-1):
        if lays_in(bins, i, val):
            return i

    # we have not located the bin in trajectrory that include our current pos
    return -1


def lays_in(bins, idx, val):
    if bins[idx] == bins[idx+1] and bins[idx] == val:
        return True

    if bins[idx] > bins[idx+1]:
        # decreasing sequence
        if val <= bins[idx] and val > bins[idx+1]:
            return True
        else:
            return False
    else:
        # increasing sequence
        if val >= bins[idx] and val < bins[idx+1]:
            return True
        else:
            return False


tpi = 2.*math.pi

directions = {}
directions['elbow_pitch'] = -1.
directions['shoulder'] = 1.

offsets = {}
offsets['elbow_pitch'] = 0
offsets['shoulder'] = math.pi / 2.

arm = Actuator(['z', [0.3, 0., 0.0], 'z', [0.064, 0., 0.], 'y', [0.4, 0., 0.]],
               bounds=((0, None), (None, -0.1), (0, None)))


def get_ee_position():
    ang0, res = bus.get_position('shoulder')
    ang1, res = bus.get_position('elbow_pitch')

    arm.angles = np.array([
        ang0 * directions['shoulder'] + offsets['shoulder'],
        ang1 * directions['elbow_pitch'] + offsets['elbow_pitch'],
        0.0
        ])

    pos3d = arm.ee

    print(f"Pos: {pos3d[0]:4F} {pos3d[1]:4F} Ang: {ang0:4F} {ang1:4F} Servo Ang: {arm.angles[0]:4F} {arm.angles[1]:4F}")
    return pos3d


if __name__ == '__main__':

    bus = DynaBus("/dev/tty.usbserial-FT5NY874", 1000000)

    bus.add_servo(3, 'PH42_SERIES', 'elbow_pitch')
    bus.add_servo(4, 'PH42_SERIES', 'elbow_yaw')
    bus.add_servo(5, 'X_SERIES', 'wrist_yaw')
    bus.add_servo(6, 'PH42_SERIES', 'wrist_roll')
    bus.add_servo(7, 'X_SERIES', 'gripper')
    bus.add_servo(43, 'PH54_SERIES', 'shoulder')

    home_pos = {}
    home_pos['elbow_pitch'] = 2.6
    home_pos['elbow_yaw'] = -0.087
    home_pos['wrist_yaw'] = 0.0330
    home_pos['wrist_roll'] = 0.0205
    home_pos['gripper'] = -0.5485
    home_pos['shoulder'] = 1.0

    # go home
    for servo in bus.servos:
        bus.set_mode(servo, 3)
        bus.set_profile(servo, 1.0, 0.2)
        bus.set_pwm_limit(servo, 0.2)
        # print(bus.get_position(servo))
        bus.enable_torque(servo, True)
        bus.set_position(servo, home_pos[servo])

    # while True:
    #     print(bus.get_position('shoulder'))
    #     sleep(0.2)

    # switch to velocity mode AND disable fng protection
    for servo in bus.servos:
        # bus.set_pwm_limit(servo, 0.25)
        bus.enable_torque(servo, False)
        bus.set_mode(servo, 1)
        bus.disable_protection(servo)
        bus.enable_torque(servo, True)

    x_fwd_col={}
    x_rev_col={}
    v_fwd_col = {}
    v_rev_col={}

    # general tragectory params
    dt = 0.04           # time tick [s]
    # max_accel = 0.5     # max accel [/ss]
    # max_jerk  = 1.0     # max jerk [/sss]
    max_accel = 4.0     # max accel [/ss]
    max_jerk  = 12.0     # max jerk [/sss]
    tm_min = 0.5    # search space for full trajectory (sec)
    tm_max = 5.0    # search space for full trajectory (sec)

    # elbow_pitch
    sx = home_pos['elbow_pitch']  # start elbow
    sv = -0.03       # start speed rad/s / 2pi
    sa = 0.0        # start accel [m/ss]
    gx = 1.2       # goal x position []
    gv = 0.01        # goal speed []
    ga = 0.00        # goal accel []

    ttt, x, v, a, j = servo_quintic_planner(sx, sv, sa, gx, gv, ga, max_accel, max_jerk, tm_min, tm_max, dt)
    _, x_rev, v_rev, _, _ = servo_quintic_planner(gx, gv, ga, sx, sv, sa, max_accel, max_jerk, tm_min, tm_max, dt)
    # x_fwd_col['elbow_pitch'] = x
    # x_rev_col['elbow_pitch'] = x_rev
    # v_fwd_col['elbow_pitch'] = v
    # v_rev_col['elbow_pitch'] = v_rev

    N = len(x)
    precision_start = 0.1
    current_plot = []

    # traj_x_list = [x_fwd_col, x_rev_col]
    # traj_v_list = [v_fwd_col, v_rev_col]

    traj_x_list = [x, x_rev]*1
    traj_v_list = [v, v_rev]*1

    # arm.angles = [1.58590792, -1.7766988, 0]
    # print(arm.ee)

    for xxx, vvv in zip(traj_x_list,traj_v_list):
        print(f"Start trajectory N={N} from pos={xxx[0]:3F}")
        traj_idx = -1
        while True:
            do_sleep = False
            pos, res = bus.get_position('elbow_pitch')
            # if we just started, we could be outside of the first bin. So, pretend we're there IF we're close to
            if traj_idx == -1:
                bin = locate(xxx, 0, pos)
                if bin == -1:
                    # we 're close enough to the starting position
                    if abs(pos-xxx[0]) < precision_start:
                        bus.set_velocity('elbow_pitch', vvv[0])
                        do_sleep = True
                        # sleep(dt/2)
                        continue
                    else:
                        bus.set_velocity('elbow_pitch', 0.0)
                        do_sleep = True
                        # sleep(dt/2)
                        print("Incorrect starting position - out of trajectory")
                        break
                else:
                    traj_idx += 1

                    continue
            else:
                bin = locate(xxx, traj_idx, pos)
                tmp = get_ee_position()
                # print(f"Bin: {bin} Pos: {get_ee_position()}")

                if bin >= N-2:
                    print(f'Trajectory Ended, pos: {pos:3F}')
                    break

                if bin < 0:
                    print(f'Out of trajectory: trj {x[traj_idx]:3F}, pos {pos:3F}, idx {traj_idx}')
                    do_sleep = True
                    # sleep(dt/4)
                    break

                # within an interval
                traj_idx = bin
                bus.set_velocity('elbow_pitch', vvv[traj_idx])
                # sleep(dt/2)
                do_sleep = True

            if do_sleep:
                sleep(dt / 2)

    bus.set_velocity('elbow_pitch', 0)  # stop
