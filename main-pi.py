from time import sleep, time
import numpy as np
from colorama import Fore

from adr_arm import Arm
from dyna import DynaBus

import rclpy
from rclpy.node import Node
from geometry_msgs.msg import Twist


deg = np.pi / 180.


class Subscriber(Node):

    def __init__(self):
        super().__init__('arm_test')
        self.subscription = self.create_subscription(
            Twist, 'cmd_vel',
            self.listener_callback,
            10)
        # self.subscription  # prevent unused variable warning

    def listener_callback(self, msg):
        # self.get_logger().info(f'twist {msg.linear.x}')
        print(f'twist {msg.data.linear.x}')



if __name__ == '__main__':

    dt_main = 0.06

    # ========= bus ==========
    my_bus = DynaBus("/dev/ttyUSB0", 2000000, max_temp=60.0)

    my_bus.add_servo(43, 'PH54_SERIES', 'shoulder', max_torque=0.2)
    my_bus.add_servo(3, 'PH42_SERIES', 'elbow_pitch', max_torque=0.4)
    my_bus.add_servo(4, 'PH42_SERIES', 'elbow_yaw', max_torque=0.2)
    my_bus.add_servo(53, 'PH42_SERIES', 'wrist_yaw', max_torque=0.2)
    my_bus.add_servo(52, 'PH42_SERIES', 'wrist_pitch', max_torque=0.2)
    my_bus.add_servo(6, 'PH42_SERIES', 'wrist_roll', max_torque=0.2)
    my_bus.add_servo(7, 'X_SERIES', 'gripper', max_torque=0.8)

    # limiting the operation stage of the end effector [m]
    ee_stage = {
        'fw_start':  0.25,
        'fw_finish': 0.65,
        'side_start': -0.16,
        'side_finish': 0.16,
        'up_start': -.16,
        'up_finish': 0.43,
        'twist_range': np.pi/2.,   # +- pi
    }

    arm = Arm(my_bus, my_bus.servos, ee_stage, dt_main)


    # uncomment to drive servos manually and check the position read
    # my_bus.do_polling = True
    # for servo in my_bus.servos:
    #     print(servo+ "  ", end='')
    # print()
    # while True:
    #     for servo in my_bus.servos:
    #         tmp = my_bus.servo_position[servo]
    #         print(Fore.BLUE + f"{tmp:.3f}\t", end='')
    #     print()
    #     sleep(0.6)


    # sleep(1)            # we must wait here! otherwise the servos screwed
    # arm.home()
    # arm.velocity_mode()
    # sleep(0.8)
    # arm.calibrate_gripper()

    rclpy.init(args=None)
    # rospy.on_shutdown(self._stop_motors)

    minimal_subscriber = Subscriber()

    rclpy.spin(minimal_subscriber)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    minimal_subscriber.destroy_node()
    rclpy.shutdown()

    sleep(66)

    exit()
