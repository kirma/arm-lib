import math
import time
from time import sleep
from DynamixelSDK.python.src.dynamixel_sdk import COMM_SUCCESS
from dyna import DynaBus, PROF_ACCEL_MAX, VELOCITY_MAX_RADS
import numpy as np
import matplotlib.pyplot as plt

from kik import Actuator
from path_planning import servo_quintic_planner

'''
Velocity control with IK with polynomial path planning
'''

precision_end = 0.1
precision_start = 0.15



# locate which viapoint we're on
# idx_min: our last viapoint
# retuns the idx of the bin or -1 if not found
def locate(bins, idx_min, val):
    for i in range (idx_min, len(bins)-1):
        if lays_in(bins, i, val):
            return i

    # if we close to the end, we still OK
    if abs(bins[-1] - val) < precision_end:
        return len(bins)

    # we have not located the bin in trajectrory that include our current pos
    return -1


def lays_in(bins, idx, val):
    if bins[idx] == bins[idx+1] and bins[idx] == val:
        return True

    if bins[idx] > bins[idx+1]:
        # decreasing sequence
        if val <= bins[idx] and val > bins[idx+1]:
            return True
        else:
            return False
    else:
        # increasing sequence
        if val >= bins[idx] and val < bins[idx+1]:
            return True
        else:
            return False


# tpi = 2.*math.pi

directions = {}
directions['elbow_pitch'] = -1.
directions['shoulder'] = 1.

offsets = {}
offsets['elbow_pitch'] = 0
offsets['shoulder'] = math.pi / 2.


def get_ee_position():
    ang0, res = bus.get_position('shoulder')
    ang1, res = bus.get_position('elbow_pitch')
    arm.angles = np.array([
        ang0 * directions['shoulder'] + offsets['shoulder'],
        ang1 * directions['elbow_pitch'] + offsets['elbow_pitch'],
        0.0
        ])
    pos3d = arm.ee
    # print(f"Pos: {pos3d[0]:4F} {pos3d[1]:4F} Ang: {ang0:4F} {ang1:4F} Servo Ang: {arm.angles[0]:4F} {arm.angles[1]:4F}")
    return pos3d


def set_servo_velocities(ee_velocity):
    vvv_ang = arm.get_ang_vel(ee_velocity)
    bus.set_velocity('shoulder', vvv_ang[0] * directions['shoulder'])
    bus.set_velocity('elbow_pitch', vvv_ang[1] * directions['elbow_pitch'])


if __name__ == '__main__':

    arm = Actuator(['z', [0.3, 0., 0.0], 'z', [0.064, 0., 0.], 'y', [0.4, 0., 0.]],
                   bounds=((0, None), (None, -0.1), (0, None)))

    bus = DynaBus("/dev/tty.usbserial-FT5NY874", 1000000)

    bus.add_servo(3, 'PH42_SERIES', 'elbow_pitch')
    bus.add_servo(4, 'PH42_SERIES', 'elbow_yaw')
    bus.add_servo(5, 'X_SERIES', 'wrist_yaw')
    bus.add_servo(6, 'PH42_SERIES', 'wrist_roll')
    bus.add_servo(7, 'X_SERIES', 'gripper')
    bus.add_servo(43, 'PH54_SERIES', 'shoulder')

    home_pos = {}
    home_pos['elbow_pitch'] = 2.6
    home_pos['elbow_yaw'] = -0.087
    home_pos['wrist_yaw'] = 0.0330
    home_pos['wrist_roll'] = 0.0205
    home_pos['gripper'] = -0.5485
    home_pos['shoulder'] = 1.0

    # go home
    for servo in bus.servos:
        bus.enable_torque(servo, False)
        bus.set_mode(servo, 3)
        bus.set_profile(servo, 1.0, 0.2)
        bus.set_pwm_limit(servo, 0.2)
        bus.enable_torque(servo, True)
        bus.set_position(servo, home_pos[servo])
    # bus.set_mode('elbow_pitch', 1)
    while True:
        if abs(bus.get_position('shoulder')[0] - home_pos['shoulder']) < precision_start:
            break
        else:
            sleep(0.3)

    # switch to velocity mode AND disable fng protection
    for servo in bus.servos:
        bus.enable_torque(servo, False)
        bus.set_mode(servo, 1)
        bus.disable_protection(servo)
        bus.enable_torque(servo, True)

    # general tragectory params
    pos_y = 0.6
    pos_z = 0.0

    dt = 0.04           # time tick [s]
    max_accel = 1.0     # max accel [/ss]
    max_jerk  = 3.0     # max jerk [/sss]
    tm_min = 0.5    # search space for full trajectory (sec)
    tm_max = 5.0    # search space for full trajectory (sec)

    # end point
    sx = 0.20
    sv = 0.06       # start speed m/s
    sa = 0.0        # start accel [m/ss]
    gx = 0.60       # goal x position []
    gv = -0.06        # goal speed []
    ga = 0.00        # goal accel []

    ttt, x, v, a, j = servo_quintic_planner(sx, sv, sa, gx, gv, ga, max_accel, max_jerk, tm_min, tm_max, dt)
    _, x_rev, v_rev, _, _ = servo_quintic_planner(gx, gv, ga, sx, sv, sa, max_accel, max_jerk, tm_min, tm_max, dt)

    N = len(x)
    current_plot = []

    traj_x_list = [x, x_rev]*5
    traj_v_list = [v, v_rev]*5

    # bin = 0
    for xxx, vvv in zip(traj_x_list,traj_v_list):
        print(f"Start trajectory N={N} from pos={xxx[0]:3F}")
        traj_idx = -1
        while True:
            do_sleep = False
            pos3d = get_ee_position()
            pos_x = pos3d[0]
            # print(f"Real: {pos_x} Expected: {xxx[bin]}")

            # if we just started, we could be outside of the first bin. So, pretend we're there IF we're close to
            if traj_idx == -1:
                bin = locate(xxx, 0, pos_x)

                if bin == -1:
                    # we 're close enough to the starting position
                    if abs(pos_x-xxx[0]) < precision_start:
                        set_servo_velocities(np.array([vvv[0], 0., 0.]))
                        do_sleep = True
                        # sleep(dt/2)
                        continue
                    else:
                        bus.set_velocity('shoulder', 0.0)
                        bus.set_velocity('elbow_pitch', 0.0)
                        do_sleep = True
                        # sleep(dt/2)
                        print("Incorrect starting position - out of trajectory")
                        break
                elif bin == N:
                    # we passed the end point
                    print(f'Trajectory ended with overshoot, pos: {pos_x:3F}')
                    break
                else:
                    traj_idx += 1
                    continue
            else:
                bin = locate(xxx, traj_idx, pos_x)

                if bin >= N-2:
                    print(f'Trajectory Ended, pos: {pos_x:3F}')
                    break

                if bin < 0:
                    print(f'Out of trajectory: trj {x[traj_idx]:3F}, pos {pos_x:3F}, idx {traj_idx}')
                    do_sleep = True
                    # sleep(dt/4)
                    break

                # within an interval
                traj_idx = bin
                set_servo_velocities(np.array([vvv[traj_idx], 0., 0.]))
                # sleep(dt/2)
                do_sleep = True

            if do_sleep:
                sleep(dt / 2)

    bus.set_velocity('shoulder', 0.0)
    bus.set_velocity('elbow_pitch', 0.0)  # stop
