from time import sleep

from DynamixelSDK.python.src.dynamixel_sdk import COMM_SUCCESS
from dyna import DynaBus, PROF_ACCEL_MAX, VELOCITY_MAX_RADS
import numpy as np
import matplotlib.pyplot as plt


if __name__ == '__main__':

    bus = DynaBus("/dev/tty.usbserial-FT5NY874", 1000000)

    bus.add_servo(3, 'P_SERIES', 'elbow_pitch')
    bus.add_servo(4, 'P_SERIES', 'elbow_yaw')
    bus.add_servo(5, 'X_SERIES', 'wrist_yaw')
    bus.add_servo(6, 'P_SERIES', 'wrist_roll')
    bus.add_servo(7, 'X_SERIES', 'gripper')
    bus.add_servo(43, 'P_SERIES', 'shoulder')

    home_pos = {}
    home_pos['elbow_pitch'] = 0.9191
    home_pos['elbow_yaw'] = 0.4862
    home_pos['wrist_yaw'] = 0.5062
    home_pos['wrist_roll'] = 0.5033
    home_pos['gripper'] = 0.4129
    home_pos['shoulder'] = 0.7741

    # go home
    for servo in bus.servos:
        bus.set_mode(servo, 3)
        bus.set_profile(servo, 1.0, 0.2)
        bus.set_pwm_limit(servo, 0.9)
        bus.enable_torque(servo, True)
        bus.set_position(servo, home_pos[servo])

    sleep(1)
    stat_cur = []
    stat_pos = []
    pos_accuracy = 0.001  # [units: normalised to 1.0]
    for i in range(5):
        p = 0.693
        pos, cur = bus.goto_position('elbow_pitch', p, pos_accuracy, True)
        stat_cur += cur
        stat_pos += pos
        p = 0.910
        pos, cur = bus.goto_position('elbow_pitch', p, pos_accuracy, True)
        stat_cur += cur
        stat_pos += pos

    # plt.plot(stat_cur, 'b')
    plt.plot(stat_cur, 'b')
    plt.show()