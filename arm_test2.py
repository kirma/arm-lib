from time import sleep
import numpy as np
import math
import hid      # pip install hidapi
from dyna import DynaBus
from kik import Actuator
from path_planning import servo_quintic_planner

precision_end = 0.1
precision_start = 0.03
# ee_control_precision = 4

stage_x_start = 0.2
stage_y_start = 0.0    # ??????? TODO: fix this
stage_x_finish = 0.5
stage_z_start = -.2
stage_z_finish = 0.2

dt = 0.05           # time tick [s]
max_accel = 1.0     # max accel [/ss]
max_jerk = 3.0      # max jerk [/sss]
tm_min = 0.1        # search space for full trajectory (sec)
tm_max = 6.0        # search space for full trajectory (sec)


# locate which viapoint we're on (N points). Fuzzy start/stop
# idx_min: our last viapoint (must be N-2 at most).
# TODO: If we did not find from the idx_min to the end, should we search from 0??
# returns: the idx of the bin or -1 if not found
# if the value just behind the end, then retutns N
# we cannot use np.digitize ad the trajectory can be non-monotonic
def locate(bins, val, idx_min=0):
    if idx_min == -2:   # out of trajectory
        idx_min = 0

    if idx_min == -1:   # just started
        if abs(bins[0] - val) < precision_start:
            return 0
        else:
            return -1
        # else - keep searching

    if idx_min == len(bins)-1:    # the last
        if  lays_in(bins, idx_min, val):
            return idx_min
        else:
            return idx_min+1

    for i in range(idx_min, len(bins)):
        if lays_in(bins, i, val):
            return i

    # we have not located the bin in trajectrory starting from idx_min
    # lets have another try from 0
    for i in range(0, idx_min):
        if lays_in(bins, i, val):
            return i

    # we have not located at all
    return -2

# fuzzy start/end of trajectory
def lays_in(bins, idx, val):

    if idx == 0:
        if bins[0] > bins[1]:
            if val >= bins[0] and abs(bins[0] - val) < precision_start:
                return True
            else:
                return False
        else:
            if val <= bins[0] and abs(bins[0] - val) < precision_start:
                return True
            else:
                return False

    elif idx < len(bins)-1:
        # not the last element of bins
        if bins[idx] > bins[idx + 1]:
            # decreasing sequence
            if val <= bins[idx] and val >= bins[idx + 1]:
                return True
            else:
                return False
        else:
            # increasing sequence
            if val >= bins[idx] and val <= bins[idx + 1]:
                return True
            else:
                return False
    else:
        # the last
        if abs(bins[-1] - val) < precision_end:
            return True
        else:
            return False


directions = {}
directions['elbow_pitch'] = -1.
directions['shoulder'] = 1.

offsets = {}
offsets['elbow_pitch'] = 0
offsets['shoulder'] = math.pi / 2.


def control_to_eepos(ctl_x, ctl_z):
    x_des_ee = ctl_x / 255. * (stage_x_finish - stage_x_start) + stage_x_start
    z_des_ee = 0.0  # TODO: fix these
    return x_des_ee, z_des_ee


def get_ee_position():
    ang0, res = bus.get_position('shoulder')
    ang1, res = bus.get_position('elbow_pitch')
    arm.angles = np.array([
        ang0 * directions['shoulder'] + offsets['shoulder'],
        ang1 * directions['elbow_pitch'] + offsets['elbow_pitch'],
        0.0
        ])
    pos3d = arm.ee
    # print(f"Pos: {pos3d[0]:4F} {pos3d[1]:4F} Ang: {ang0:4F} {ang1:4F} Servo Ang: {arm.angles[0]:4F} {arm.angles[1]:4F}")
    return pos3d


def set_servo_velocities(ee_velocity):
    vvv_ang = arm.get_ang_vel(ee_velocity)
    bus.set_velocity('shoulder', vvv_ang[0] * directions['shoulder'])
    bus.set_velocity('elbow_pitch', vvv_ang[1] * directions['elbow_pitch'])


# === respect the limits ===
def check_limits(pos, velo, desired_velo):
    if pos <= stage_x_start:
        if desired_velo < 0 or velo < 0:
            bus.stop_velocity()
            print(f"stopped 2! {pos}")
            return False
    if pos >= stage_x_finish:
        if desired_velo > 0 or velo > 0:
            bus.stop_velocity()
            print(f"stopped 3! {pos}")
            return False
    return True


if __name__ == "__main__":
    for device in hid.enumerate():
        print(f"0x{device['vendor_id']:04x}, 0x{device['product_id']:04x} {device['product_string']}")
        if device['product_string'].find('Controller') >= 0:
            vendor_id = device['vendor_id']
            product_id = device['product_id']

    if not vendor_id:
        raise Exception("No controller found!")

    gamepad = hid.device()
    gamepad.open(vendor_id, product_id)
    gamepad.set_nonblocking(True)


    arm = Actuator(['z', [0.3, 0., 0.0], 'z', [0.064, 0., 0.], 'y', [0.4, 0., 0.]],
                   bounds=((0, None), (None, -0.1), (0, None)))

    bus = DynaBus("/dev/tty.usbserial-FT5NY874", 1000000)

    bus.add_servo(3, 'PH42_SERIES', 'elbow_pitch')
    bus.add_servo(4, 'PH42_SERIES', 'elbow_yaw')
    bus.add_servo(5, 'X_SERIES', 'wrist_yaw')
    bus.add_servo(6, 'PH42_SERIES', 'wrist_roll')
    bus.add_servo(7, 'X_SERIES', 'gripper')
    bus.add_servo(43, 'PH54_SERIES', 'shoulder')

    home_pos = {}
    home_pos['elbow_pitch'] = 2.5
    home_pos['elbow_yaw'] = 0.0
    home_pos['wrist_yaw'] = 0.0
    home_pos['wrist_roll'] = 0.0
    home_pos['gripper'] = -0.2
    home_pos['shoulder'] = 1.0

    # go home
    for servo in bus.servos:
        bus.enable_torque(servo, False)
        bus.set_mode(servo, 3)
        bus.set_profile(servo, 1.0, 0.2)
        bus.set_pwm_limit(servo, 0.2)
        bus.enable_torque(servo, True)
        bus.set_position(servo, home_pos[servo])
    while True:
        if abs(bus.get_position('shoulder')[0] - home_pos['shoulder']) < precision_start:
            break
        else:
            sleep(0.3)

    # switch to velocity mode AND disable fng protection
    for servo in bus.servos:
        bus.enable_torque(servo, False)
        bus.set_mode(servo, 1)
        bus.disable_protection(servo)
        bus.enable_torque(servo, True)

    traj_idx = -1

    pos3d = get_ee_position()
    pos_x = pos3d[0]
    vel_x = 0.0

    ctl_x = 127
    ctl_z = 127
    desired_x, desired_z = control_to_eepos(ctl_x, ctl_z)
    old_ctl_x = -999
    t, x, v, a, j = servo_quintic_planner(pos_x, vel_x, 0, desired_x, 0, 0, max_accel, max_jerk, tm_min, tm_max, dt)
    print(f"N = {len(x)}")

    while True:
        # TODO: update control input here
        old_ctl_x = ctl_x

        channels = gamepad.read(64)
        if channels:
            ctl_x = float(255 - channels[4])
            ctl_z = float(channels[3])

        # update pos/vel
        pos3d = get_ee_position()
        vel_x = vel_x * 0.3 + 0.7 * (pos3d[0] - pos_x) / dt
        pos_x = pos3d[0]

        # if control changed, we re-calc new trajectory
        if abs(old_ctl_x - ctl_x) > 12:
            # if abs(desired_x - pos_x) > 0.03:
            desired_x, desired_z = control_to_eepos(ctl_x, ctl_z)
            _, x, v, a, j = servo_quintic_planner(pos_x, vel_x, 0, desired_x, 0, 0, max_accel, max_jerk, tm_min, tm_max, dt)
            traj_idx = -1
        else:
            traj_idx = locate(x, pos_x, traj_idx)
            # print(f"bin: {traj_idx} pos: {pos_x:3f} x: {x[0]:3f} {x[1]:3f} {x[2]:3f} {x[3]:3f} {x[4]:3f}")


        if traj_idx == len(x):
            bus.stop_velocity()
            print(f"overshoot the traj: {pos_x}")
        elif traj_idx == len(x)-1:
            bus.stop_velocity()
            print(f"got to the end: {pos_x}")
        elif traj_idx == -2:
            bus.stop_velocity()
            print(f"got lost: {pos_x}")
        elif traj_idx == -1:
            desired_vx = v[0]
            if check_limits(pos_x,vel_x,desired_vx):
                set_servo_velocities(np.array([desired_vx, 0., 0.]))
        else:
            desired_vx = v[traj_idx]
            if check_limits(pos_x,vel_x,desired_vx):
                set_servo_velocities(np.array([desired_vx, 0., 0.]))

        print(f"IK pos {pos_x} bin {traj_idx}")

        sleep(dt)
