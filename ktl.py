from timeloop import Timeloop

# Timeloop object allows to periodically call a procedure with high precision and protection
# https://medium.com/greedygame-engineering/an-elegant-way-to-run-periodic-tasks-in-python-61b7c477b679
# the problem is, it does not pass arguments, so we cannot use it inside a class. We extend this with "self" passing thru

class SuperTimeloop(Timeloop):
    def job(self, interval, *args, **kwargs):
        def decorator(f):
            self._add_job(f, interval, *args, **kwargs)
            return f

        return decorator
