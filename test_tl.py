from datetime import timedelta
from time import sleep

from ktl import SuperTimeloop

class TestMe(object):
    def __init__(self):

        self.aaa = 0

        tl = SuperTimeloop()

        @tl.job(timedelta(milliseconds=900), self)
        def polling_servo_job(obj):
            obj.aaa += 1
            print(f"New aaa {obj.aaa}")

        tl.start(block=False)


if __name__ == "__main__":

    C = TestMe()

    while True:
        sleep(1)
