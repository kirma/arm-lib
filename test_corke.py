from time import sleep, time

import hid
import numpy as np
from roboticstoolbox import tools
from roboticstoolbox.robot.ET import ET
from roboticstoolbox.robot.ERobot import ERobot
from roboticstoolbox.robot.Link import Link
from spatialmath import SE3, SO3

deg = np.pi / 180.
mm = 1e-3


# ======= joystick =======
class Joy(object):
    def __init__(self):
        for device in hid.enumerate():
            print(f"0x{device['vendor_id']:04x}, 0x{device['product_id']:04x} {device['product_string']}")
            if device['product_string'].find('Controller') >= 0:
                vendor_id = device['vendor_id']
                product_id = device['product_id']

        if not vendor_id:
            raise Exception("No controller found!")

        self.gamepad = hid.device()
        self.gamepad.open(vendor_id, product_id)
        self.gamepad.set_nonblocking(True)

        self.ctl_x, self.ctl_y, self.ctl_z, self.ctl_r = 0., 0., 0.0, 0.

    def read(self):
        channels = self.gamepad.read(64)
        if channels:
            self.ctl_x = float(255 - channels[4] - 127) / 5000.
            self.ctl_y = float(channels[3] - 127) / 5000.
            self.ctl_z = float(255 - channels[2] - 127) / 5000.
            self.ctl_r = float(channels[1] - 127) / 5000.

            if abs(self.ctl_x) < 0.001:
                self.ctl_x = 0.

            if abs(self.ctl_y) < 0.001:
                self.ctl_y = 0.

            if abs(self.ctl_z) < 0.001:
                self.ctl_z = 0.

            if abs(self.ctl_r) < 0.001:
                self.ctl_r = 0.

        return self.ctl_x, self.ctl_y, self.ctl_z, self.ctl_r


class Arm(ERobot):

    def __init__(self):

        l0 = Link(ET.tz(0.010) * ET.Rx(), name="base", parent=None, qlim=(-60.*deg, 60.*deg))
        l1 = Link(ET.tz(0.200) * ET.Rx(), name="link1", parent=l0, qlim=(-150.*deg, 0.*deg))
        l2 = Link(ET.tz(0.064) * ET.Ry(), name="link2", parent=l1)
        l3 = Link(ET.tz(0.180) * ET.Ry(), name="link3", parent=l2)
        l4 = Link(ET.tz(0.060) * ET.Rx(), name="link4", parent=l3, qlim=[-80.*deg, 45.*deg])
        l5 = Link(ET.tz(0.120) * ET.Rz(), name="link5", parent=l4)
        # l6 = Link(ET.tz(0.200) * ET.Rz(), name="fake", parent=l5)
        ee = Link(ET.tz(0.200), name="ee", parent=l5)

        elinks = [l0, l1, l2, l3, l4, l5, ee]

        super(Arm, self).__init__(elinks, name="ADR Arm", manufacturer="ADR")

        self.home = np.array([45*deg, -135*deg, 0*deg, 0*deg, 0*deg, 0])
        T = self.fkine(self.home)
        # self.R = T.R
        self.R = np.array([
            [ 1.,  0.,  0.],
            [ 0.,  0.,  1.],
            [ 0., -1.,  0.]])
        print(self.R)
        self.ee_old_pos = T.t
        self.q_old = self.home

        # self.world_orientation = SE3(T.A * SE3(T.t).inv())
        # SE3(SO3(np.array([[1, 0, 0], [0, 0, 1], [0, 1, 0]]))
        # self.addconfiguration("home", self.home)

    # take into account the desired ee orientation
    # ee_rot is the rotation of the ee along the vertical axis (left/right)
    def solve_ik(self, ee_pos, ee_rot):
        sol = self.ikine_LM(SE3(ee_pos) * SE3(SO3(self.R)) * SE3(SO3().Ry(ee_rot)), tol=1e-3, q0=self.q_old)
        # sol = self.ikine_min(SE3(ee_pos) * SE3(SO3(self.R)) * SE3(SO3().Ry(ee_rot)), q0=self.q_old, tol=1e-4, method='Nelder-Mead')  # 0.056
        # sol = self.ikine_min(SE3(ee_pos) * SE3(SO3(self.R)) * SE3(SO3().Ry(ee_rot)), q0=self.q_old, tol=1e-3, method='TNC')       # 0.018
        # sol = self.ikine_min(SE3(ee_pos) * SE3(SO3(self.R)) * SE3(SO3().Ry(ee_rot)), q0=self.q_old, tol=1e-3, method='Powell')      # 0.200
        # sol = self.ikine_min(SE3(ee_pos) * SE3(SO3(self.R)) * SE3(SO3().Ry(ee_rot)), q0=self.q_old, stiffness=0.1,  tol=1e-3, method='L-BFGS-B')
        if sol.success:
            self.q_old = sol.q

        return sol



if __name__ == "__main__":
    robot = Arm()
    print(robot)

    joy = Joy()

    # T = robot.fkine(robot.home)
    # print(robot.home)
    # T = SE3(0., 0.14257864, 0.15142136)
    # old_pos = robot.ikine_LM(T, tol=3e-3)

    # print(old_pos.q)
    joy_x, joy_y, joy_z, joy_r = joy.read()

    ee_new_pos = robot.ee_old_pos
    old_joints_sol = robot.solve_ik(ee_new_pos, 0*deg)

    # robot.plot(robot.home, jointaxes=True, limits=np.array([-0.3, 0.3, -0.3, 0.3, 0, 0.6]))
    robot.plot(old_joints_sol.q, jointaxes=True, limits=np.array([-0.3, 0.3, -0.3, 0.3, 0, 0.6]))
    print()
    rot = 0.
    while True:
        joy_x, joy_y, joy_z, joy_r = joy.read()
        # new_T = T * SE3(joy_y, joy_z, joy_x)
        # ee_new_pos = ee_new_pos + np.array([0.0, 0.02, 0.0])
        ee_new_pos = ee_new_pos + np.array([-joy_y, joy_x, joy_z])
        rot = rot + joy_r

        ttt = time()
        new_joints_sol = robot.solve_ik(ee_new_pos, rot)
        print(f"{time()-ttt:.4F}")

        # print(rot)
        # print(ee_new_pos)

        # new_T = T * SE3(0, -0.02, 0.0)
        # side, up, forward
        # new_pos = robot.ikine_LM(new_T, tol=1e-2)
        if not new_joints_sol.success:
            print("No joy!")
        else:
            # calc velocities thru built in trajectory
            qt = tools.trajectory.jtraj(old_joints_sol.q, new_joints_sol.q, 3)
            # print(qt.qd[1])
            old_joints_sol = new_joints_sol
            # T = new_T

        robot.plot(old_joints_sol.q, jointaxes=True, limits=np.array([-0.3, 0.3, -0.3, 0.3, 0, 0.6]))


        # print(new_pos.q)
        # print(T.t)
        sleep(0.1)



