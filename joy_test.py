from time import sleep

import hid      # pip install hidapi

if __name__ == "__main__":
    for device in hid.enumerate():
        print(f"0x{device['vendor_id']:04x}, 0x{device['product_id']:04x} {device['product_string']}")

    gamepad = hid.device()
    gamepad.open(0x054c, 0x09cc)
    gamepad.set_nonblocking(True)

    while True:
        report = gamepad.read(23)
        if report:
            for r in report[0:23]:
                print(f"\t{r}", end='')
            print()
            # print(report)
            if (report[5] & 0b100000) != 0:
                print("X")
            sleep(0.2)


"""
struct PS4InputReport
{
    byte reportId;             // #0
    byte leftStickX;           // #1
    byte leftStickY;           // #2
    byte rightStickX;          // #3
    byte rightStickY;          // #4
    byte dpad : 4;             // #5 bit #0 (0=up, 2=right, 4=down, 6=left)
    byte squareButton : 1;     // #5 bit #4
    byte crossButton : 1;      // #5 bit #5
    byte circleButton : 1;     // #5 bit #6
    byte triangleButton : 1;   // #5 bit #7
    byte leftShoulder : 1;     // #6 bit #0
    byte rightShoulder : 1;    // #6 bit #1
    byte leftTriggerButton : 2;// #6 bit #2
    byte rightTriggerButton : 2;// #6 bit #3
    byte shareButton : 1;      // #6 bit #4
    byte optionsButton : 1;    // #6 bit #5
    byte leftStickPress : 1;   // #6 bit #6
    byte rightStickPress : 1;  // #6 bit #7
    byte psButton : 1;         // #7 bit #0
    byte touchpadPress : 1;    // #7 bit #1
    byte padding : 6;
    byte leftTrigger;          // #8
    byte rightTrigger;         // #9
}

"""